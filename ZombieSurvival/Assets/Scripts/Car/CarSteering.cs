﻿using System.Collections;
using UnityEngine;

public class CarSteering : MonoBehaviour
{
    #region Variables 

    /// <summary> The range in wich player is detected </summary>
    [Tooltip(" The range in wich player is detected ")]
    [SerializeField] private float range = 20;
    /// <summary> The layer of the player </summary>
    [Tooltip(" The layer of the player ")]
    [SerializeField] private LayerMask playerLayer;
    /// <summary> Aceleration power that will be used to add force to the car´s Rigidbody2D </summary>
    [Tooltip(" Aceleration power that will be used to add force to the car´s Rigidbody2D ")]
    [SerializeField] private float accelerationPower = 5f;
    /// <summary> Steering power that will be used to add rotation to the car´s Rigidbody2D </summary>
    [Tooltip(" Steering power that will be used to add rotation to the car´s Rigidbody2D ")]
    [SerializeField] private float steeringPower = 5f;
    /// <summary> The car's light. To turn on when player is driving </summary>
    [Tooltip(" The car's light. To turn on when player is driving ")]
    [SerializeField] private GameObject lights;

    [SerializeField] private TankCanon tankCanon;

    /// <summary>  The collider in the the car sprite </summary>
    [Tooltip(" The collider in the the car sprite ")]
    [SerializeField] private PolygonCollider2D carCollider;


    /// <summary> Car movement variable </summary>
    private float steeringAmount, direction;
    /// <summary> Car speed </summary>
    private float speed;
    /// <summary> Car speed penalty caused because car damage. From 0 to 1(0% and 100%) </summary>
    [HideInInspector] public float speedPenalty = 0;
    /// <summary> RigidBody2D component of the car </summary>
    [HideInInspector]public Rigidbody2D rb;
    /// <summary> Is the player driving the car ? </summary>
    [HideInInspector] public bool driving;
    /// <summary> The player´s PlayerMovementController component </summary>
    private PlayerMovementController player;

    [SerializeField] private Transform wheelStepParent;
    [SerializeField] private GameObject carTrack;
    [SerializeField] private Transform wheels;
    [SerializeField] private SpriteRenderer darkSprite;
    Color initialColor;
    #endregion

    #region Methods


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(WaitForPlayerGetClose());
        speedPenalty = 0;
        initialColor = darkSprite.color;
    }

    Vector2 lastPos;
    float dist = 0.5f;

    private void Update()
    {
        if(driving)
            if (Vector2.Distance(transform.position, lastPos) > dist)
            {
                Debug.Log(wheels.childCount);
                for(byte i =0; i < wheels.childCount;++i)
                {
                    lastPos = transform.position;
                    Vector3 instantiatePos = new Vector3( wheels.GetChild(i).position.x, wheels.GetChild(i).position.y, -0.5f);
                    GameObject carStep = Instantiate(carTrack, instantiatePos, Quaternion.identity);
                    carStep.transform.parent = wheelStepParent;
                    carStep.transform.eulerAngles = (new Vector3(0, 0, transform.eulerAngles.z + 90));
                }
            }
    }

   
    /// <summary> Wait until the player enter to the car clicking the right mouse button </summary>
    private IEnumerator WaitEnterCar()
    {
        float range = Vector2.Distance(transform.position, player.transform.position);
        do
        {
            if (Input.GetKey(KeyCode.M))
            {
                driving = true;
                player.StopMovement(transform);
                darkSprite.color = Color.white;
                Camera.main.GetComponent<Animator>().SetBool("Zoom", false);
                Camera.main.GetComponent<FollowTarget>().target = transform;
                ZombieTarget.target = transform;
                Zombie.target = transform;
                lights.gameObject.SetActive(true);
                if (tankCanon != null)
                    tankCanon.enabled = true;
                StartCoroutine(WaitExitCar());
                yield break;
            }
            yield return new WaitForEndOfFrame();
        } while (Vector2.Distance(transform.position, player.transform.position) <= range);
        StartCoroutine(WaitForPlayerGetClose());
    }

    private IEnumerator WaitExitCar()
    {
        
        while (true)
        {
            if (Input.GetKey(KeyCode.N))
            {
                driving = false;
                player.StartMovement();
                darkSprite.color = initialColor;
                Camera.main.GetComponent<Animator>().SetBool("Zoom", true);
                Camera.main.GetComponent<FollowTarget>().target = ZombieTarget.target = Zombie.target = player.transform;
                lights.gameObject.SetActive(false);
                if (tankCanon != null)
                    tankCanon.enabled = false;
                carCollider.isTrigger = true;
                StartCoroutine(WaitEnterCar());
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
            carCollider.isTrigger = false;
    }


    /// <summary> Wait until the player get close to the car </summary>
    private IEnumerator WaitForPlayerGetClose()
    {
        while (true)
        {
            Collider2D playerCollider = Physics2D.OverlapCircle(transform.position, range, playerLayer);
            if (playerCollider)
            {
                player = playerCollider.GetComponent<PlayerMovementController>();
                StartCoroutine(WaitEnterCar());
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary> Set the car´s movement and rotation from player´s input </summary>
    void FixedUpdate()
    {
        if (driving)
        {
            steeringAmount = -Input.GetAxis("Horizontal");
            speed = Input.GetAxis("Vertical") * accelerationPower;
            if (rb.velocity.magnitude > 1 || rb.velocity.magnitude < -1) transform.position = new Vector3(transform.position.x, transform.position.y, -2);
            else transform.position = new Vector3(transform.position.x, transform.position.y, -0.8f);
            direction = Mathf.Sign(Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up)));
            rb.rotation += steeringAmount * steeringPower * rb.velocity.magnitude * direction;
            rb.AddRelativeForce((Vector2.up * speed)  - (Vector2.up * speed * new Vector2(speedPenalty,speedPenalty)) );

            rb.AddRelativeForce(-Vector2.right * rb.velocity.magnitude * steeringAmount / 2);
        }
    }

    private void OnDrawGizmos()
    {
        //draws the range in which player is detected around the car
        Gizmos.DrawWireSphere(transform.position, range);
    }

    #endregion
}