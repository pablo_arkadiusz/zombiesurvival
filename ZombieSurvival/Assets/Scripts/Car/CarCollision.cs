﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollision : MonoBehaviour
{
    private CarLife carLife;

    private void Start()
    {
        carLife = transform.parent.parent.GetComponent<CarLife>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(!other.gameObject.CompareTag("Player"))
            carLife.TakeDamage(5);
    }



    void Update ()
    {
		
	}
}
