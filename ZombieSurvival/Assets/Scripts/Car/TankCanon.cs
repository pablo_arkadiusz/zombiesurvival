﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCanon : MonoBehaviour
{
    private const float carCooldown = 0.1f;
    private const float tankCooldown = 1f;

    private bool cooldown;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float damage;
    [SerializeField] private Transform CanonFirePosition;

    [SerializeField] private bool tank;



	void Update ()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));

        Vector2 facingDirection = -new Vector2
        (
            mousePos.x - transform.position.x,
            mousePos.y - transform.position.y
         );

        if (facingDirection.x > 0.2f || facingDirection.x < 0.2f) //Bugg: stange Y-axis flip. TEMPORAL SOLUTION
            transform.up = -facingDirection;



        if (Input.GetButton("Fire1"))
        {
            if (tank)
            {
                if (!cooldown)
                {
                    cooldown = true;
                    StartCoroutine(ChargeCooldown(tankCooldown));
                    ExplodingBullet bullet = Instantiate(bulletPrefab, CanonFirePosition.position, Quaternion.identity).GetComponent<ExplodingBullet>();
                    bullet.Damage = damage;
                    bullet.finalPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
                    bullet.SetRotation(new Vector3(0, 0, transform.eulerAngles.z + 90));
                }
            }
            else
            {
                if (!cooldown)
                {
                    cooldown = true;
                    StartCoroutine(ChargeCooldown(carCooldown));
                    Bullet bullet = Instantiate(bulletPrefab, CanonFirePosition.position, Quaternion.identity).GetComponent<Bullet>();
                    bullet.Damage = damage;
                    bullet.SetRotation(new Vector3(0, 0, transform.eulerAngles.z - 90));
                }
            }
        }
    }


    private IEnumerator ChargeCooldown(float cdTime)
    {
        yield return new WaitForSeconds(cdTime);
        cooldown = false;
    }


}
