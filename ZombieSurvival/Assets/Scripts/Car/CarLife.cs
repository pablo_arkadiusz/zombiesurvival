﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarLife : MonoBehaviour
{
    [SerializeField] private Sprite spriteLevel3;
    [SerializeField] private Sprite spriteLevel2;
    [SerializeField] private Sprite spriteLevel1;
    [SerializeField] private Sprite spriteLevel0;


    [SerializeField] private float life;


    [SerializeField] private SpriteRenderer carSpriteRend, blackSpriteRend;

    private CarSteering carSteering;

    private byte spriteLevel=4;

    private void Start()
    {
        carSteering = GetComponent<CarSteering>();
    }


    public void TakeDamage(float damage)
    {
        life -= damage;
        switch (spriteLevel)
        {
            case 4:
                if (life <= 75)
                {
                    --spriteLevel;
                    carSteering.speedPenalty = 0.25f;
                    carSpriteRend.sprite = blackSpriteRend.sprite = spriteLevel3;
                }
                break;
            case 3:
                if(life<=50)
                {
                    --spriteLevel;
                    carSteering.speedPenalty = 0.45f;
                    carSpriteRend.sprite = blackSpriteRend.sprite = spriteLevel2;
                }
                break;
            case 2:
                if (life <= 25)
                {
                    --spriteLevel;
                    carSteering.speedPenalty = 0.7f;
                    carSpriteRend.sprite = blackSpriteRend.sprite = spriteLevel1;
                }
                break;
            case 1:
                if (life <= 0)
                {
                    carSteering.speedPenalty = 1f;
                    carSpriteRend.sprite = blackSpriteRend.sprite = spriteLevel0;
                }
                break;
        }

    }
}
