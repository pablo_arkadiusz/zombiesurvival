﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Bullet instantiated firing a granade or tank bullet </summary>
public class ExplodingBullet : MonoBehaviour
{
    #region Variables

    /// <summary> Movement speed of the bullet </summary>
    [Tooltip(" Movement speed of the bullet ")]
    [SerializeField] private float moveSpeed = 10f;

    /// <summary> Damage of the bullet, setted during the bullet instantiation (PlayerAttackController.cs) </summary>
    [HideInInspector] public float Damage { set; private get; }

    [SerializeField] private float explosionArea;
    [SerializeField] private LayerMask enemyLayer;

    /// <summary> Direction of the bullet setted during the bullet instantiation to the direction that hte player is looking </summary>
    private Vector3 direction;

    [HideInInspector]public Vector2 finalPosition;
    #endregion

    #region Methods

    // Move the bullet in the left direction (is the forward site of the sprite)
    private void Update()
    {
        transform.Translate(transform.right * moveSpeed * Time.deltaTime, Space.World);
        if (Vector2.Distance(transform.position, finalPosition) < 1)
        {
            moveSpeed = 0;
            GetComponent<Animator>().enabled = true;
        }
    }

    public void SetRotation(Vector3 rotation) { transform.eulerAngles = rotation; }

    //It only collide/trigger with enemy layer
    private void OnTriggerEnter2D(Collider2D other)
    {
        moveSpeed = 0;
        GetComponent<Animator>().enabled = true;
    }

    public void DestroyBullet()
    {
        Destroy(gameObject);
    }


    public void DoDamageInArea()
    {
        //Gets the enemies in the range depending of the weapon that player has equipped
        Collider2D[] enemiesInAttackRange;

            enemiesInAttackRange = Physics2D.OverlapCircleAll(transform.position, explosionArea, enemyLayer);
        

        //Do damage to each of the enemies in the range
        foreach (Collider2D enemyCollider in enemiesInAttackRange)
            enemyCollider.GetComponent<Zombie>().TakeDamage(Damage);
    }



    private void OnDrawGizmos()
    {
        //Draws explosion attack area
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, explosionArea);

       
    }


    #endregion
}