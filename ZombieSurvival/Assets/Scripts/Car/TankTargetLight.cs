﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankTargetLight : MonoBehaviour
{

    
	
	void Update ()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));

        transform.position = new Vector3(mousePos.x, mousePos.y, transform.position.z);
    }
}
