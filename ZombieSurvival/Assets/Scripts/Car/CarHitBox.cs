﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarHitBox : MonoBehaviour
{

    private const byte enemyLayer = 8;
    private CarSteering car;

    private CarLife carLife;

	void Start ()
    {
        car = GetComponent<CarSteering>();
        carLife = GetComponent<CarLife>();
	}
	

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == enemyLayer)
            if (car.rb.velocity.magnitude > 1 || car.rb.velocity.magnitude < -5)
                other.GetComponent<Zombie>().TakeDamage(200);
    }



}
