﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToCollect : MonoBehaviour
{

    [SerializeField] private SelectWeaponSystem weaponMenu;

    [SerializeField] private Weapons typeOfItem;

   
    public void GetItem()
    {
        weaponMenu.TryAddWeapon(typeOfItem);
        Destroy(gameObject);
    }

 
}
