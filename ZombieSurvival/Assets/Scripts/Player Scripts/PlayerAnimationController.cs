﻿using UnityEngine;

/// <summary> Script that manages player animations </summary>
public class PlayerAnimationController : MonoBehaviour
{
    /// <summary> Animator component of the player character </summary>
    private Animator myAnimator;

    private void Awake() {myAnimator = GetComponent(typeof(Animator)) as Animator;}

    //Methods that changes the variables of the player's animator
    public void StartWalkAnimation(){ myAnimator.SetBool("Walking", true); }
    public void StopWalkAnimation(){ myAnimator.SetBool("Walking", false); }
    public void StartFireAnimation() { myAnimator.SetBool("Attacking", true);}
    public void StopFireAnimation() { myAnimator.SetBool("Attacking", false);}
    public void PlayDieAnimation() { myAnimator.SetTrigger("Die");}

    /// <summary> Set off the last weapon and turn on the new one </summary>
    public void ChangeWeapon(string weaponName, string lastWeapon)
    {
        myAnimator.SetBool(lastWeapon, false);
        myAnimator.SetBool(weaponName, true);
        myAnimator.SetTrigger("ChangeWeapon");
    }
}
