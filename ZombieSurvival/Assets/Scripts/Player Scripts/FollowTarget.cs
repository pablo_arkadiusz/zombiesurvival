﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Script for the camera, to follow the player´s position </summary>
public class FollowTarget : MonoBehaviour
{
    /// <summary> The transform of the player to get his position </summary>
    [Tooltip(" The transform of the player to get his position ")]
    public Transform target;
    /// <summary> The position in Z axis that the camera will have </summary>
    private float zPos;

    private void Start() { zPos = transform.position.z; }

    //Moves the this object to the player´s position
    private void Update(){transform.position = new Vector3(target.position.x, target.position.y, zPos);}

}
