﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    #region Variables

    //CONST

    /// <summary> distance to process with the light raycast a new obstacle. If there are more than one obect at the "viewRange" </summary>
    private const float edgeDstThreshold = 0.1f;

    //DISTANCES

    /// <summary> Radius of player's lantern light view </summary>
    [Header("Distances")][Tooltip("Radius of player light view")]  
    public float viewRadius;
    /// <summary> Angle of player's lantern light view </summary>
    [Tooltip("Angle of player light view")] 
    [Range(0, 360)] public float viewAngle;
    /// <summary> Distance in which player can see the black sprites of enemies </summary>
    [Tooltip(" Distance in which player can see the black sprites of enemies ")] 
    [SerializeField] private float radarDistance = 30f;
    /// <summary> Distance/Margin to start the light (to see part of the obstacles). Creates a "3D" shadow effect </summary>
    [Tooltip("Distance/Margin to start the light (to see part of the obstacles). Creates a \"3D\" shadow effect")] 
    [SerializeField] private float lightCutAwayDist = .1f;

    //OPTIMIZATIONS

    /// <summary> The larger the value, the more Raycast will be in the lanter's light angle </summary>
    [Header("Optimizations")][Tooltip("The larger the value, the more Raycast will be in the lanter's light angle")]
    [SerializeField] private float meshResolution;
    /// <summary> Number of iterations to create a new raycast when the last raycast go out of a obstacle.It is used to create a new raycast between 
    /// <para> the last raycast that stopped collide with an obstacle and the next one that still is colliding.
    /// So it create a smoother transition</para></summary>
    [Tooltip("Number of iterations to create a new raycast when the last raycast go out of a obstacle.It is used to create a new raycast between " +
        "the last raycast that stopped collide with an obstacle and the next one that still is colliding. So it create a smoother transition")]
    [SerializeField] private int edgeResolverIterations;

    //LAYERS

    /// <summary> Layers to be targeted with player's lantern light (enemies, allies, items...) </summary>
    [Header("Layers")] [Tooltip("Layers to be targeted with player's lantern light (enemies, allies, items...)")]
    [SerializeField] private LayerMask targetMask;
    /// <summary> Layers that dont let cross the player's lantern light </summary>
    [Tooltip("Layers that dont let cross the player's lantern light")] 
    [SerializeField] private LayerMask obstacleMask;

    //MESHFILTER COMPONENT

    /// <summary> The MeshFilter Component of the lantern that the script will affect </summary>
    [Header("Components")][Tooltip("The MeshFilter Component of the lantern that the script will affect")] 
    [SerializeField] private MeshFilter viewMeshFilter;
    private Mesh viewMesh;

    //LISTS

    /// <summary> Objects in the targetMask that are actually targeted </summary>
    [HideInInspector] public List<Transform> visibleTargets = new List<Transform>();
    /// <summary> List with shadows of enemies that will be visible if the enemy distance is in the player's "radarDistance"  </summary>
    private List<SpriteRenderer> enemyShadowList = new List<SpriteRenderer>();

    //RADAR

    [Header("Radar")]
    [Tooltip(" Use radar to show enemies in the radar distance. Only one Field of View should activate this ")]
    /// <summary> Use radar to show enemies in the radar distance. Only one Field of View should activate this </summary>
    [SerializeField] private bool useRadar; 

    #endregion

    #region Methods

    private void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        //Start looking for targets in the radius and angle setted every 0.2 secs;
        if (useRadar) StartSearchingEnemies();
    }

    
    /// <summary> Use radar to show enemies in the radar distance. Only one Field of View should activate this </summary>
    private IEnumerator FindTargetWithDelay(float delay) 
    //Coroutine that look for targets in the radius and angle setted
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();

        }
    }

    //Also called from PlayerCombatController after the selection weapon menu stops. Because it turn off the lantern/Field Of View  
    public void StartSearchingEnemies()  
    {
        StartCoroutine(FindTargetWithDelay(.5f));
    }

    //Using LateUpdated to wait the PlayerMovement has already updated the position and rotation. So it looks smoother
    private void LateUpdate()
    {
        DrawFieldOfView();
    }


    /// <summary> 
    /// looks for objects of the "targetMask" in the player latern's light (the radius setted) and add them in the "visibleTargets" list
    /// </summary>
    private void FindVisibleTargets()
    {
        visibleTargets.Clear();
        foreach (SpriteRenderer sprite in enemyShadowList) //The black sprites of enemies
        {
            //if(sprite)the enemy could be destroyed 
                    sprite.enabled = false;
        }
        enemyShadowList.Clear();

        //if a enemy is in the radar Distance his dark sprite will be visible
        Collider2D[] targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, radarDistance, targetMask);

        //go through all found targets
        for (int i = 0; i < targetsInViewRadius.Length; ++i)
        {
            Transform target = targetsInViewRadius[i].transform;

            float distToTarget = Vector2.Distance(transform.position, target.position);

            //if target is in the radar Distance his dark sprite will be visible

            visibleTargets.Add(target);
            //The target is a enemy so make visible his "Dark sprite/material (shadow)"

            SpriteRenderer enemyShadow = target.GetChild(0).GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            enemyShadow.enabled = true;
            enemyShadowList.Add(enemyShadow);
        }
    }

    /// <summary>  Draw the lines of the light using raycast </summary>
    private void DrawFieldOfView()
    {
        //number of lines
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        //angle of each step
        float stepAngleSize = viewAngle / stepCount;

        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; ++i)
        {
            //Angle of the actual line/step is setted to follow the player forward direction
            float angle = -(transform.eulerAngles.z - viewAngle / 2 + stepAngleSize * i) + 180;

            //Add the Point of collision(in case there is any. If not the final point is the end of the light) of the actual line to the "viewPoint" list
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;

                //If one raycast is hitting the obstacle and the other isn't. Or the two are hitting but they are probably hitting different object
                //Because their distance is greather than the "edgeDstThreshold".
                //Then adds a new raycast between these two, near the obstacle edge. (this is just to improve the smoothness)
                if (oldViewCast.hit != newViewCast.hit || newViewCast.hit && oldViewCast.hit && edgeDstThresholdExceeded)
                {
                    //Adds a new raycast between the last raycast that is colliding with an obstacle and the next one that is not colliding
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero)
                        viewPoints.Add(edge.pointA);
                    if (edge.pointB != Vector3.zero)
                        viewPoints.Add(edge.pointB);
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;

        }
        //The vertexCount is the number of vertex that are in the triangles without repeating EXAMPLE: 3 triangles will have 5 vertex
        int vertexCount = viewPoints.Count + 1; //+1 because of the origin point
        //Array with the position of all the vertex
        Vector3[] vertices = new Vector3[vertexCount];

        //number of triangles is (vertexCount-2). EXAMPLE:(one triangle have 3 vertex. So 3-2 = 1 triangle)
        //The number of triangles * 3 is the total of vertex inluding the vertex that repeat in the triangles that are side by side.
        int[] triangles = new int[(vertexCount - 2) * 3];

        //the first vertex is in the player position (the local position (0,0,0))
        vertices[0] = Vector3.zero;

        //go through all vertex
        for (int i = 0; i < vertexCount - 1; ++i)
        {
            //Set the ending point of the vertex (if it has collide with the layer "obstacleMask" it will be shorter) (Setted in Line 91)
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]) - Vector3.up * lightCutAwayDist; //transform.InverseTransformPoint is to use local position


            if (i < vertexCount - 2) //to dont go out of range
            {
                //This set the numbers of each vertex of the triangles
                //For example if there are 3 triangles it would be: [0,1,2,0,2,3,0,3,4]
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        Array.Reverse(triangles); //Error: The mesh was backwards, so this flip the mesh and solve the problem

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    /// <summary> 
    /// Finds the Edge of a collider to create a new raycast between the last raycast of the lantern's light that is colliding and the next one that is not colliding
    /// </summary>
    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) // this method is used just to improve the smoothness
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        //Find EdgeInfo to create a new raycast between the last raycast that stopped collide with an obstacle and the next one that still is colliding
        //So the light transition is smoother
        for (int i = 0; i < edgeResolverIterations; ++i)
        //This loop narrow the distance between the raycast that left the obstacle and the one next to it that is still colliding
        //the bigger "edgeResolverIterations" is, the closer it gets to the edge of the obstacle
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }
        return new EdgeInfo(minPoint, maxPoint);
    }

    /// <summary> Return Info about the limit point of the raycast / ViewPoint of the players' lantern light raycast </summary>
    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask);
        //If it collide with object of "obstacleMask" layer this return the information about that collision point
        //It is used for example to set the lantern's light distance of raycast to that collision point
        //Physics2D.Raycast(transform.position, dir, out hit, viewRadius, obstacleMask)
        return Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask) ?
            new ViewCastInfo(true, hit.point, hit.distance, globalAngle)
            :
            new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
    }


    /// <summary>  Returns the direction where the angle will point. (the player's latern light will point to the face of the player) </summary>
    public Vector2 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        //If angle is static his direction is always going top (the direction of angle 0 is (0,1))
        //If angle is not global, mean that his direction is relative (depends where the object is facing)
        if (!angleIsGlobal)
            //the direction of angle is the Z-axis rotation (Example: The latern's light direction is the direction where the player looks)
            angleInDegrees -= transform.eulerAngles.z;
        return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    private void OnDrawGizmos()
    {
        //Draws the distance of the radar
        Gizmos.DrawWireSphere(transform.position, radarDistance);
    }

    #endregion

    #region structs used to organice variables

    /// <summary> Contains the information about the raycasts of the light </summary>
    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    /// <summary> Struct used to find edge of an obstacle </summary>
    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }

    #endregion
}
