﻿using System.Collections.Generic;
using UnityEngine;

/// <summary> Script that manages the collider of the fire from the Firethrower weapon. It accumulates all the enemy in the fire range </summary>
public class FireColliders : MonoBehaviour
{

    /// <summary> List with all the enemies in the fire range </summary>
    List<Collider2D> colliders = new List<Collider2D>();

    //On trigger with enemy add him to the collider List 
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!colliders.Contains(other))
            colliders.Add(other);
    }

    //On trigger exit remove the enemy from list
    private void OnTriggerExit2D(Collider2D other)
    {
        if (colliders.Contains(other))
            colliders.Remove(other);
    }

    /// <summary> Returns all the enemy in the fire range </summary>
    public Collider2D[] GetColliders() // called from DoDamageInArea() in PlayerCombatController.cs
    {
        return colliders.ToArray();
    }
}

