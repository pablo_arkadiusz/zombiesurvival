﻿using UnityEngine;

/// <summary> Bullet instantiated firing a riffle and gun </summary>
public class Bullet : MonoBehaviour
{
    #region Variables

    /// <summary> Movement speed of the bullet </summary>
    [Tooltip(" Movement speed of the bullet ")]
    [SerializeField] private float moveSpeed = 10f;

    /// <summary> Damage of the bullet, setted during the bullet instantiation (PlayerAttackController.cs) </summary>
    [HideInInspector] public float Damage { set; private get; }

    /// <summary> Direction of the bullet setted during the bullet instantiation to the direction that hte player is looking </summary>
    private Vector3 direction;

    #endregion

    #region Methods

    // Move the bullet in the left direction (is the forward site of the sprite)
    private void Update(){transform.Translate(-transform.right * moveSpeed * Time.deltaTime, Space.World);}

    public void SetRotation(Vector3 rotation) {transform.eulerAngles = rotation;}

    //It only collide/trigger with enemy layer
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other)
            other.GetComponent<Zombie>().TakeDamage(Damage);
        moveSpeed = 0;
        GetComponent<Animator>().enabled = true;
    }

    public void DestroyBullet()
    {
        Destroy(gameObject);
    }

    #endregion
}