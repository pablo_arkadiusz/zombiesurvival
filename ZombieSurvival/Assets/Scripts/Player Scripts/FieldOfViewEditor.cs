﻿using UnityEngine;
using UnityEditor;

//Class that shows on editor different stuff for each FieldOfViewItem
[CustomEditor (typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    private void OnSceneGUI()
    {
        //The field of the view(fow) is the player's lintern light
        FieldOfView fow = (FieldOfView)target;
        Handles.color = Color.white;

        //Draws a circle in the inspector around the player's character (the distance of the player's lintern light)
        Handles.DrawWireArc(fow.transform.position, Vector3.forward, Vector3.up, 360, fow.viewRadius);

        //Two lines with the positive and negative angle of view of the player's lintern light
        Vector2 viewAngleA = fow.DirFromAngle(-fow.viewAngle / 2, false);
        Vector2 viewAngleB = fow.DirFromAngle(fow.viewAngle / 2, false);

        //Draws two lines. From player to the circle limit (the distance of the player's lintern light) Its the actual player's lintern light area.
        Handles.DrawLine(fow.transform.position, (Vector2)fow.transform.position + viewAngleA * fow.viewRadius);
        Handles.DrawLine(fow.transform.position, (Vector2)fow.transform.position + viewAngleB * fow.viewRadius);

        //Draws a line from player to every visible targets
        foreach(Transform visibleTarget in fow.visibleTargets)
            Handles.DrawLine(fow.transform.position, visibleTarget.transform.position);
    }
}
