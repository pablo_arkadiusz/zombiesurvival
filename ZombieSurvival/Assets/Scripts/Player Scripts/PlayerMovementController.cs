﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary> Class that manages the player character movement and rotation logic </summary>
public class PlayerMovementController : MonoBehaviour
{
    #region Variables

    #region Movement variables

    /// <summary> The player character movement speed </summary>
    
    [Header("Movement")] [Tooltip("The player character movement speed")]
    [SerializeField] private float moveSpeed = 6;
    private float initialMoveSpeed;

    /// <summary> The direction of player movement </summary>
    private Vector2 moveDirection;

    /// <summary> Is player walking ? </summary> Used to efficiency and dont use invoke some methods when player is not walking
    private bool walking;

    /// <summary> Direction of player movement </summary> 
    private bool walkingTop, walkingRight;
    private bool isDrivingCar;
    #endregion

    #region Energy Variables

    /// <summary> The limit of energy the player can have </summary>
    private const float maxEnergy = 100;

    /// <summary> The time of player run </summary>
    [Header("Energy")] [Tooltip("The time of player run")]
    [SerializeField] private float energyTime = 4;

    /// <summary> The time needed to charge the full energy bar </summary>
    [Tooltip("The time needed to charge the full energy bar")]
    [SerializeField] private float energyChargeTime = 8;

    /// <summary> The energy to run/sprint </summary>
    private float actualEnergy;

    /// <summary> Is player running ? </summary>
    private bool running;

    #endregion



    #region Player's component variables

    /// <summary> The energy bar to fill and unfill when player runs </summary>
    [Header("Components")] [Tooltip("The energy bar to fill and unfill when player runs")]
    [SerializeField] private Image energyBar;

    /// <summary> Player's RigidBody2D </summary>
    private Rigidbody2D myRigidbody;

    /// <summary> PlayerAnimationController Component. Used To start and stop walking animations </summary>
    private PlayerAnimationController animationController;

    /// <summary> Player's transform</summary>
    private Transform myTransform;

    #endregion

    #endregion

    #region Methods

    void Start()
    {
        actualEnergy = maxEnergy;
        initialMoveSpeed = moveSpeed;
        myRigidbody = GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        animationController = GetComponent(typeof(PlayerAnimationController)) as PlayerAnimationController;
        myTransform = GetComponent(typeof(Transform)) as Transform;


    }


    private void Update()
    {
        if (!isDrivingCar)
        {
            MovementLogic();
            RotateTowardsMouse();

            if (Input.GetKeyDown(KeyCode.LeftShift) && actualEnergy > 0)
                Sprint();
        }
    }

    private void FixedUpdate()
    {
        if (walking && !isDrivingCar)
            myRigidbody.MovePosition(UpdateMovementDirection());
    }
    

    /// <summary> Returns the next position of the player's rigidbody </summary>
    private Vector2 UpdateMovementDirection()
    {
        float horizonalDir, verticalDir;
        //Sets the velocity from the player's input
        horizonalDir = walkingRight ? Input.GetAxisRaw("Right") : Input.GetAxisRaw("Left");
        verticalDir = walkingTop ? Input.GetAxisRaw("Up") : Input.GetAxisRaw("Down");

        moveDirection = new Vector2(horizonalDir, verticalDir).normalized * moveSpeed;

        return myRigidbody.position + moveDirection * Time.fixedDeltaTime;
    }

    /// <summary> Rotate player facing the mouse </summary>
    private void RotateTowardsMouse()
    {
        //Transforms position of mouse from Screen space into World space.
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));

        Vector2 facingDirection = -new Vector2
        (
            mousePos.x - transform.position.x,
            mousePos.y - transform.position.y
         );

        if (facingDirection.x > 0.2f || facingDirection.x <0.2f) //TODO: Bugg: stange Y-axis flip. TEMPORAL SOLUTION
            transform.up = facingDirection;
    }

    private void Sprint()
    {
        running = true;
        moveSpeed *= 2;
        StartCoroutine(LosingEnergy());
    }

    private void StopSprint()
    {
        running = false;
        moveSpeed = initialMoveSpeed;
    }

    private IEnumerator LosingEnergy()
    {
        float valueToSubstract = maxEnergy / energyTime * Time.deltaTime;
        while(Input.GetKey(KeyCode.LeftShift) && actualEnergy > 0)
        {
            actualEnergy -= valueToSubstract;
            energyBar.fillAmount = actualEnergy / 100;
            yield return new WaitForEndOfFrame();
        }
        StopSprint();
        StartCoroutine(ChargingEnergy());
    }

    private IEnumerator ChargingEnergy()
    {
        float valueToAdd = maxEnergy / energyChargeTime * Time.deltaTime;

        while (actualEnergy<maxEnergy && !running)
        {
            actualEnergy += valueToAdd;

            energyBar.fillAmount = actualEnergy / 100;
            yield return new WaitForEndOfFrame();
        }
        
    }


    /// <summary> Set the logic of the movement bools and the walk animations </summary>
    private void MovementLogic()
    //The movement is not made by the simple Horizontal and Vertical axis because it needs to walk even if the player press A and D at the same time.
    //So the input add four axis more: Left, Up, Right and Down
    //The player movement follows the first given direction. For example when click the key D and at the same time the key  A. Player goes Left until the D key is released, 
    //then goes to right if A is still pressed.
    {
        #region Horizontal movement

        if (Input.GetButtonDown("Left") && !Input.GetButton("Right"))
        {
           
            walking = true;
            walkingRight = false;
            animationController.StartWalkAnimation();
        }
        else if (Input.GetButtonDown("Right") && !Input.GetButton("Left"))
        {
            walking = true;
            walkingRight = true;
            animationController.StartWalkAnimation();
        }

        else if (walking)
        {
            if (Input.GetButtonUp("Left"))
            {
                walkingRight = true;
                if (!Input.GetButton("Right") && !Input.GetButton("Vertical"))
                {
                    walking = false;
                    animationController.StopWalkAnimation();
                }
            }
            else if (Input.GetButtonUp("Right"))
            {
                walkingRight = false;
                if (!Input.GetButton("Left") && !Input.GetButton("Vertical"))
                {
                    walking = false;
                    animationController.StopWalkAnimation();
                }
            }
        }

        #endregion

        #region Vertical movement

        if (Input.GetButtonDown("Up") && !Input.GetButton("Down"))
        {
            walking = true;
            walkingTop = true;
            animationController.StartWalkAnimation();
        }
        else if (Input.GetButtonDown("Down") && !Input.GetButton("Up"))
        {
            walking = true;
            walkingTop = false;
            animationController.StartWalkAnimation();
        }
        else if (walking)
            if (Input.GetButtonUp("Up"))
            {
                walkingTop = false;
                if (!Input.GetButton("Down") && !Input.GetButton("Horizontal"))
                {
                    walking = false;
                    animationController.StopWalkAnimation();
                }
            }
            else if (Input.GetButtonUp("Down"))
            {
                walkingTop = true;
                if (!Input.GetButton("Up") && !Input.GetButton("Horizontal"))
                {
                    walking = false;
                    animationController.StopWalkAnimation();
                }
            }
        #endregion
    }

    public void StopMovement(Transform car)//when player is in the car it stop the standart movement.
    {
        GetComponent<PlayerCombatController>().enabled = false;
        carToFollow = car;
        isDrivingCar = true;
        GetComponent<SpriteRenderer>().enabled = false;
        myRigidbody.simulated = false;
        StartFollowingCar();
    }


    public void StartMovement()
    {
        GetComponent<PlayerCombatController>().enabled = true;

        isDrivingCar = false;
        GetComponent<SpriteRenderer>().enabled = true;
        myRigidbody.simulated = true;
        StartFollowingCar();
    }

    private void StartFollowingCar()
    {
        StartCoroutine(FollowCar());
    }

    private Transform carToFollow;
    private IEnumerator FollowCar()
    {
        while(isDrivingCar)
        {
            transform.position = new Vector3(carToFollow.position.x, carToFollow.position.y,transform.position.z);
            yield return new WaitForEndOfFrame();
        }
    }


#endregion
}
