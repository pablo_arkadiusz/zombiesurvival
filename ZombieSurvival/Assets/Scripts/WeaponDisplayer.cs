﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WeaponDisplayer : MonoBehaviour
{
    #region Variables

    [Header(" Weapon Prefabs ")]
    [SerializeField] private GameObject batPrefab;
    [SerializeField] private GameObject fireThrowerPrefab, granadePrefab, gunPrefab, knifePrefab, rifflePrefab, lightPrefab, barrelPrefab;
    [Header(" Text ")]
    [SerializeField] private Text ammoText;
    [Header(" Images ")]
    [SerializeField] private Image bulletFillAmmoImage;
    [SerializeField] private Image backgroundAmmoImage;
    [SerializeField] private Image fireFillAmmoImage;

    /// <summary> The background panel of the WeaponDisplayer, to set the parent of the displayer weapon</summary>
    private Transform backgroundPanel;
    /// <summary> The weapon that is actually displayed </summary>
    private GameObject actualDisplayed;
    /// <summary> The player Combat Controller. To get The actual weapon that the player has </summary>
    private PlayerCombatController player;

    #endregion

    #region Methods

    private void Start()
    {
        backgroundPanel = transform.GetChild(0);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCombatController>();
    }

    /// <summary> Destroys the previous displayed Weapon and instantiates the new one, setting visible or invisible the needed elements </summary>
    public void ChangeDisplayedWeapon(Weapons weapon)
    {
        Destroy(actualDisplayed);
        switch (weapon)
        {
            case Weapons.Bat:
                //Creates the new weapon to display and set the parent
                GameObject bat = Instantiate(batPrefab, backgroundPanel.position, Quaternion.identity);
                bat.transform.SetParent(backgroundPanel);
                actualDisplayed = bat;
                //The bat dont need the ammunition or the text elements
                SetVisibleElements(false, false);
                break;

            case Weapons.FireThrower:
                //Creates the new weapon to display and set the parent
                GameObject fireThrower = Instantiate(fireThrowerPrefab, backgroundPanel.position, Quaternion.identity);
                fireThrower.transform.SetParent(backgroundPanel);
                actualDisplayed = fireThrower;
                //The FireThrower needs the text to see the amount of ammunition and the fire elements visible
                SetVisibleElements(true, false, true);
                //Updates the bar of the FireThrower ammunition
                UpdateAmmoBar();
                //Updates the stack of the fire ammunition
                UpdateAmmoStack();
                break;

            case Weapons.Granade:
                //Creates the new weapon to display and set the parent
                GameObject granade = Instantiate(granadePrefab, backgroundPanel.position, Quaternion.identity);
                granade.transform.SetParent(backgroundPanel);
                actualDisplayed = granade;
                //The Granade just need the text to see the amount of granades the player has
                SetVisibleElements(true, false);
                //Updates the stack of the granades
                UpdateAmmoStack();
                break;

            case Weapons.Gun:
                //Creates the new weapon to display and set the parent
                GameObject gun = Instantiate(gunPrefab, backgroundPanel.position, Quaternion.identity);
                gun.transform.SetParent(backgroundPanel);
                actualDisplayed = gun;
                //The gun needs the text and the gun ammo elements visible
                SetVisibleElements(true, true);
                //Updates the Gun´s ammunition bar
                UpdateAmmoBar();
                //Updates the stack of the gun ammunition
                UpdateAmmoStack();
                break;

            case Weapons.Knife:
                //Creates the new weapon to display and set the parent
                GameObject knife = Instantiate(knifePrefab, backgroundPanel.position, Quaternion.identity);
                knife.transform.SetParent(backgroundPanel);
                actualDisplayed = knife;
                //The knife needs the text and the gun ammo elements visible
                SetVisibleElements(false, false);
                break;

            case Weapons.Riffle:
                //Creates the new weapon to display and set the parent
                GameObject riffle = Instantiate(rifflePrefab, backgroundPanel.position, Quaternion.identity);
                riffle.transform.SetParent(backgroundPanel);
                actualDisplayed = riffle;
                //The riffle needs the text and the gun ammo elements visible
                SetVisibleElements(true, true);
                //Updates the Riffle´s ammunition bar
                UpdateAmmoBar();
                //Updates the stack of the riffle ammunition
                UpdateAmmoStack();
                break;

            case Weapons.Light:
                //Creates the new weapon to display and set the parent
                GameObject light = Instantiate(lightPrefab, backgroundPanel.position, Quaternion.identity);
                light.transform.SetParent(backgroundPanel);
                actualDisplayed = light;
                //The Light just need the text to see the amount of lights the player has
                SetVisibleElements(true, false);
                //Updates the stack of the light ammunition
                UpdateAmmoStack();
                break;

            case Weapons.Barrel:
                //Creates the new weapon to display and set the parent
                GameObject barrel = Instantiate(barrelPrefab, backgroundPanel.position, Quaternion.identity);
                barrel.transform.SetParent(backgroundPanel);
                actualDisplayed = barrel;
                //The Barrel just need the text to see the amount of barrel the player has
                SetVisibleElements(true, false);
                //Updates the stack of the barrel ammunition
                UpdateAmmoStack();
                break;
        }
    }

    /// <summary> Updates the bar of the weapon that the player has equipped </summary>
    public void UpdateAmmoBar() //called on weapon change and when player shoot
    {
        if (player.ActualWeapon == Weapons.Gun)
            bulletFillAmmoImage.fillAmount = (player.weaponAmmo.gunAmmo / (float)WeaponAmmo.maxAmmo);
        else if (player.ActualWeapon == Weapons.Riffle)
            bulletFillAmmoImage.fillAmount = (player.weaponAmmo.riffleAmmo / (float)WeaponAmmo.maxAmmo);
        else if (player.ActualWeapon == Weapons.FireThrower)
            fireFillAmmoImage.fillAmount = (player.weaponAmmo.fireThrowerAmmo / (float)WeaponAmmo.maxAmmo);
    }

    /// <summary> Updates the stack of the ammunition of the weapon that the player has equipped </summary>
    public void UpdateAmmoStack()
    {
        if (player.ActualWeapon == Weapons.Gun)
            ammoText.text = "X " + player.weaponAmmo.gunAmmoStack.ToString();
        else if (player.ActualWeapon == Weapons.Riffle)
            ammoText.text = "X " + player.weaponAmmo.riffleAmmoStack.ToString();
        else if (player.ActualWeapon == Weapons.FireThrower)
            ammoText.text = "X " + player.weaponAmmo.fireAmmoStack.ToString();
        else if (player.ActualWeapon == Weapons.Granade)
            ammoText.text = "X " + player.weaponAmmo.granadeStack.ToString();
        else if (player.ActualWeapon == Weapons.Light)
            ammoText.text = "X " + player.weaponAmmo.lightStack.ToString();
        else if (player.ActualWeapon == Weapons.Barrel)
            ammoText.text = "X " + player.weaponAmmo.barrelStack.ToString();
    }

    /// <summary> Set visible or invisible the WeaponDisplayer elements.(stack text, gun-ammo and firethrower elements) </summary>  
    /// <param name="textVisible"> Should be the stack ammo text visible ?? </param> <param name="ammoVisible"> Should be the gun ammo elements visible ?? </param> <param name="fireVisible"> Should be the fire ammo elements visible ?? </param>
    private void SetVisibleElements(bool textVisible, bool ammoVisible, bool fireVisible = false)
    {
        ammoText.enabled = textVisible;
        bulletFillAmmoImage.enabled = ammoVisible;
        fireFillAmmoImage.enabled = fireVisible;
        backgroundAmmoImage.enabled = fireVisible || ammoVisible;
    }

    #endregion
}