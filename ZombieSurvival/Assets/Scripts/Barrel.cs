﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{

    [HideInInspector] public bool isPlaced;
    [SerializeField] private float damage;

    [SerializeField] private float explosionArea;
    [SerializeField] private LayerMask enemyLayer;
    private Transform placedBarrels;

    void Start ()
    {
        placedBarrels = GameObject.FindGameObjectWithTag("PlacedBarrels").transform;
	}


    public void DoDamageInArea()
    {



        Collider2D[] enemiesInAttackRange = Physics2D.OverlapCircleAll(transform.position, explosionArea, enemyLayer);

        Debug.Log("damaged: " + enemiesInAttackRange.Length);

        //Do damage to each of the enemies in the range
        foreach (Collider2D enemyCollider in enemiesInAttackRange)
            enemyCollider.GetComponent<Zombie>().TakeDamage(damage);
    }



    public void PlaceBarrel()
    {
        isPlaced = true;
        transform.SetParent(placedBarrels);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<Animator>().enabled = true;
    }

    public void DestroyBullet()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        //Draws explosion attack area
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, explosionArea);
    }
}
