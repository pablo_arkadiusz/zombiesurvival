﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Script that manages the mouse clicks  </summary>
public class MouseSelection : MonoBehaviour
{

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            Ray ray = new Ray(mousePos + Vector3.back, Vector3.forward); //shoot a raycast from top to down in the Z axis to get the region

            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 100); //converts the 3d raycast to 2d raycast

            if (hit) //collect the items that are clicked
            {
                ItemToCollect item = hit.collider.GetComponent<ItemToCollect>();
                if (item) item.GetItem();
            }

        }

    }
}