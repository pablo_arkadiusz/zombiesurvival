﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary> Script that manages the Weapon Selection System. It get active pressing space (in MenuManager.cs) </summary>
public class SelectWeaponSystem : MonoBehaviour
{
    #region Variables

    /// <summary> Margin of the image from the center of the menu (the objects should appear in the main circle) </summary>
    private const float distanceFromCenter = 15.5f;

    #region Other Components

    [Header("Other Components")]
    /// <summary> Player's PlayerCombatController component </summary>
    [Tooltip("Player's Player CombatController component")]
    [SerializeField] private PlayerCombatController player;
    /// <summary> Gun Displayer Component to change the weapon that is displayer in the down-left panel </summary>
    [Tooltip(" Gun Displayer Component to change the weapon that is displayer in the down-left panel ")]
    [SerializeField] private WeaponDisplayer weaponDisplayer;
    #endregion

    #region Weapon Prefabs

    // Prefabs of the gameobject with the Image of the weapon that will appear in the menu circle
    [Header("Weapon Prefabs")]
    [SerializeField] private GameObject granadePrefab;
    [SerializeField] private GameObject gunPrefab;
    [SerializeField] private GameObject rifflePrefab;
    [SerializeField] private GameObject knifePrefab;
    [SerializeField] private GameObject batPrefab;
    [SerializeField] private GameObject fireThrowerPrefab;
    [SerializeField] private GameObject medicalKitPrefab;
    [SerializeField] private GameObject lightPrefab;
    [SerializeField] private GameObject barrelPrefab;

    #endregion

    #region Rotation Circles

    /// <summary> Transform of the circle that will rotate </summary>
    [Header("Rotation Circles")] [Tooltip("Transform of the circle that will rotate")]
    [SerializeField] private Transform externalCircle;
    [SerializeField] private Transform internalCircle;
    [SerializeField] private Transform circleSmall;
    [SerializeField] private Transform center;

    #endregion

    #region Weapon Variables

    /// <summary> List with all weapons that instantiate when player start this menu. The weapons are the one that player had adquired</summary>
    private List<WeaponPlace> instantiatedWeapons = new List<WeaponPlace>();
    /// <summary> the WeaponPlace where the selected weapon is. The weapon of this WeaponPlace will be white and all other will be black </summary>
    private WeaponPlace selectedWeaponPlace = new WeaponPlace();
    /// <summary> Variable to make black again the old selected weapon </summary>
    private GameObject oldWeapon = null;
    /// <summary> The adquired weapons by player </summary>
    private Weapons disponibleWeapons;
    /// <summary> Number of weapons that player has </summary>
    private byte numWeap = 4;
    /// <summary> Angle between every weapon in the "Weapon Menu Selecter" to mantain the same distance between every weapon</summary>
    private float angleMargin; //if there are 2 weapons it will be 180, if there are 3 will be 120, etc (360 / numWeap;) 

    #endregion

    #endregion

    #region Methods 

    private void Start()
    {
        disponibleWeapons |= Weapons.Gun | Weapons.Knife | Weapons.Riffle | Weapons.Bat;
        angleMargin = 360 / numWeap; //the angle margin setted to fit all weapons mantaining the same margin distance
    }

    private void Update()
    {
        RotateCircles();
        SelectCloserWeapon(); //Select close weapon making it white, and all other black
    }

    public void TryAddWeapon(Weapons weapon)
    {
        if (!((disponibleWeapons & weapon) == weapon)) //if player dont have the weapon already
        {
            disponibleWeapons |= weapon;
            ++numWeap;
            angleMargin = 360 / numWeap; //the angle margin setted to fit all weapons mantaining the same margin distance
        }
    }

    public void SubstractItem(Weapons weapon)
    {
        if ((disponibleWeapons & weapon) == weapon) //if player already have the weapon 
        {
            disponibleWeapons &= ~weapon;
            --numWeap;
            angleMargin = 360 / numWeap;
        }
    }


    /// <summary> Return the type of the selected weapon </summary>
    public Weapons GetSelectedWeapon() //It is used to get the type of the weapon that the player needs when changing weapon
    {
        return selectedWeaponPlace.typeOfWeapon;
    }

    /// <summary> Creates weapons that covers all the Menu Circle mantaining a angleMargin betweeen each others </summary>
    public void CreateWeapons() //called when entry animation finish
    {
        //using Dictionary as Tuple (or Pair in c++) to return 2 values. Because Unity doesnt support tuples
        Dictionary<GameObject,Weapons> objectsWithTypes = SelectWeaponPrefabs(); 
        //From that pair creates a separation of the prefabs and the types of objects
        List<GameObject> objectsPrefab = new List<GameObject>(objectsWithTypes.Keys);
        List<Weapons> typeOfObjects = new List<Weapons>(objectsWithTypes.Values);

        float angle = 0;
        for (byte i = 0; i < numWeap; ++i, angle += angleMargin)
        {
            //the position to instantiate the weapon is a straight line from the center of the menu circle, in the direction of the angle of every position
            Vector3 posToInstantiate = center.position + Quaternion.Euler(0, 0, angle) * center.up * distanceFromCenter;
            //posToInstantiate -= new Vector3(0, 0, 3);
            //instantiate the weapon
            GameObject weaponCreated = Instantiate(objectsPrefab[i], posToInstantiate, Quaternion.identity);

            //Rotates the weapon facing the center position of the circle
            Vector3 difference = center.position - weaponCreated.transform.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            weaponCreated.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ+90);

            //Add the WeaponPlace to the list that collects all WeaponPlaces. The list is used to make the selected sprite white and all the rest black
            weaponCreated.transform.SetParent(transform);
            
            instantiatedWeapons.Add(new WeaponPlace(weaponCreated, angle,typeOfObjects[i]));
        }
    }

    /// <summary> Destroys the weapons</summary>
    public void DestroyWeapons() //Used when player leaves the menu selector 
    {
        foreach (WeaponPlace wp in instantiatedWeapons)
            Destroy(wp.weaponObject);
        instantiatedWeapons.Clear();
    }

    /// <summary> Search through the bytes of "disponibleWeapons" and return a dictionary with all the prefabs and the type of weapon of the weapons that player adquired </summary>
    private Dictionary<GameObject,Weapons> SelectWeaponPrefabs()
    {
        //disponibleWeapons is used as a bitwise variable
        Dictionary<GameObject, Weapons> adquiredWeaponPrefabs = new Dictionary<GameObject, Weapons>();
        if ((disponibleWeapons & Weapons.Bat) == Weapons.Bat) //if there is a Bat in the adquired weapons
            adquiredWeaponPrefabs.Add(batPrefab,Weapons.Bat);
        if ((disponibleWeapons & Weapons.FireThrower) == Weapons.FireThrower)//if there is a Firethrower in the adquired weapons
            adquiredWeaponPrefabs.Add(fireThrowerPrefab,Weapons.FireThrower);
        if ((disponibleWeapons & Weapons.Granade) == Weapons.Granade)//if there is a Granade in the adquired weapons
            adquiredWeaponPrefabs.Add(granadePrefab, Weapons.Granade);
        
        if ((disponibleWeapons & Weapons.Gun) == Weapons.Gun)//if there is a Gun in the adquired weapons
            adquiredWeaponPrefabs.Add(gunPrefab,Weapons.Gun);
        if ((disponibleWeapons & Weapons.Knife) == Weapons.Knife)//if there is a Knife in the adquired weapons
            adquiredWeaponPrefabs.Add(knifePrefab,Weapons.Knife);
        if ((disponibleWeapons & Weapons.Light) == Weapons.Light)//if there is a Light in the adquired weapons
            adquiredWeaponPrefabs.Add(lightPrefab,Weapons.Light);
        if ((disponibleWeapons & Weapons.MedicalKit) == Weapons.MedicalKit)//if there is a MedicalKit in the adquired weapons
            adquiredWeaponPrefabs.Add(medicalKitPrefab,Weapons.MedicalKit);
        if ((disponibleWeapons & Weapons.Riffle) == Weapons.Riffle)//if there is a Riffle in the adquired weapons
            adquiredWeaponPrefabs.Add(rifflePrefab, Weapons.Riffle);
        if ((disponibleWeapons & Weapons.Barrel) == Weapons.Barrel)//if there is a Barrel in the adquired weapons
            adquiredWeaponPrefabs.Add(barrelPrefab, Weapons.Barrel);

        return adquiredWeaponPrefabs; //returns all the prefabs of the weapons that player has adquired
    }

    /// <summary> Rotates the menu circles in two diferent directions</summary>
    private void RotateCircles()
    {
        Vector3 rotation = new Vector3(0, 0, 0.2f);
        externalCircle.eulerAngles += rotation;
        circleSmall.localEulerAngles += rotation;
        internalCircle.localEulerAngles -= rotation;    
    }

    /// <summary>Find the angle of WeaponPlace closest to the angle of mouse and make the weapon of that place white color</summary>
    private void SelectCloserWeapon()
    {
        //the closer angle diference between the mouse and a weaponplace.(the closer one will be selected)
        float closerAngleDiference = 360;

        foreach (WeaponPlace obj in instantiatedWeapons)
            if (MouseAngle() < 360 && MouseAngle() > 360 - angleMargin) //if the mouse is in the position between the first weaponplace and the last one
                //this condition is because 360 degree and 1 degree should be have a diference of 1 degree and not 359
            {
                if (MouseAngle() > 360 - angleMargin / 2) //if the mouse is closer to the first one
                {
                    //Changes the selectedWeaponPlace (which saves the weapon, the angle and the type of weapon of the selected WeaponPlace)
                    GameObject weapon = instantiatedWeapons[0].weaponObject;
                    float angleOfPos = instantiatedWeapons[0].angleOfThisPosition;
                    Weapons typeOfWeapon = instantiatedWeapons[0].typeOfWeapon;
                    selectedWeaponPlace = new WeaponPlace(weapon, angleOfPos, typeOfWeapon);
                    //player.ActualWeapon = GetSelectedWeapon();
                    //Updates the closer angle
                    closerAngleDiference = Mathf.Abs(MouseAngle() - obj.angleOfThisPosition);
                }
                else //if the mouse is closer to the last one
                {
                    //Changes the selectedWeaponPlace (which saves the weapon, the angle and the type of weapon of the selected WeaponPlace)
                    GameObject weapon = instantiatedWeapons[instantiatedWeapons.Count - 1].weaponObject;
                    float angleOfPos = instantiatedWeapons[instantiatedWeapons.Count - 1].angleOfThisPosition;
                    Weapons typeOfWeapon = instantiatedWeapons[instantiatedWeapons.Count - 1].typeOfWeapon;
                    selectedWeaponPlace = new WeaponPlace(weapon, angleOfPos, typeOfWeapon);
                    //player.ActualWeapon = GetSelectedWeapon();
                    //Updates the closer angle
                    closerAngleDiference = Mathf.Abs(MouseAngle() - obj.angleOfThisPosition);
                }
            }
            //if the angle between the mosue and a WeaponPlace is less than the previous one
            else if (closerAngleDiference > Mathf.Abs(MouseAngle() - obj.angleOfThisPosition))
            {
                //Changes the selectedWeaponPlace (which saves the weapon, the angle and the type of weapon of the selected WeaponPlace)
                closerAngleDiference = Mathf.Abs(MouseAngle() - obj.angleOfThisPosition);

                //Updates the closer angle
                selectedWeaponPlace = new WeaponPlace(obj.weaponObject, obj.angleOfThisPosition, obj.typeOfWeapon);
                //player.ActualWeapon = GetSelectedWeapon();
            }

        if (oldWeapon != null) //the last weapon go back to black sprite
            oldWeapon.GetComponent<Image>().color = Color.black;

        //the closest weapon to the mouse angle changes his sprite to white
        if (selectedWeaponPlace.weaponObject != null && selectedWeaponPlace.weaponObject.GetComponent<Image>().color != Color.white)
        {
            selectedWeaponPlace.weaponObject.GetComponent<Image>().color = Color.white;
            oldWeapon = selectedWeaponPlace.weaponObject;
        }
    }

    /// <summary>Returns the angle between the mouse and the center of the "Menu Circle"</summary>
    private float MouseAngle()
    {
        Vector3 menuCenterPos = Camera.main.WorldToScreenPoint(center.position);
        menuCenterPos = Input.mousePosition - menuCenterPos;
        float angle = Mathf.Atan2(menuCenterPos.y, menuCenterPos.x) * Mathf.Rad2Deg;
        angle = angle - 90;
        if (angle < 0.0f) angle += 360.0f;
        return angle;
    }

    /// <summary> Open the menu and stops the time </summary>
    public void OpenMenu()
    {
        gameObject.SetActive (true);
        player.StartTheSelectWeaponMode();
        Time.timeScale = 0;
    }

    /// <summary> Close the menu and give the selected weapon to the player </summary>
    public void CloseMenu()
    {
        player.ChangeWeapon(GetSelectedWeapon());
        weaponDisplayer.ChangeDisplayedWeapon(GetSelectedWeapon());
        DestroyWeapons();
        gameObject.SetActive(false);
        player.StopTheSelectWeaponMode();
        Time.timeScale = 1;
    }


    /// <summary> Struct with the weapon, the angle of the weapon between the center of the "Weapon Menu Selector" and the type of weapon(enum) </summary>
    public struct WeaponPlace
    {
        public GameObject weaponObject;
        public Weapons typeOfWeapon;
        public float angleOfThisPosition;

        public WeaponPlace(GameObject _weaponObject, float _angleOfThisPosition, Weapons _typeOfWeapon)
        {
            weaponObject = _weaponObject;
            angleOfThisPosition = _angleOfThisPosition;
            typeOfWeapon = _typeOfWeapon;
        }
    }

    #endregion
}
