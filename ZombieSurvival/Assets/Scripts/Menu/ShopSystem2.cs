﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSystem2 : MonoBehaviour
{
    private const byte shopPositionsLenght = 6;
    [SerializeField] private Transform[] shopPositions = new Transform[shopPositionsLenght];

    public enum ItemType
    {
        None, GunAmmo, RiffleAmmo, FireAmmo, FireThrower, Granade, Mine, Barrel, Light, MedicalKit,
        Food, Armor, SimpleCar, ArmoredCar, CompactTank, AllTerrainCar, Tank, Radar
    }
    public enum ShopCategories { Ammo, Weapons, Farmacy, CarShop, Radar }


    [Header("SHOP ITEM PREFABS")]
    [SerializeField] private GameObject gunAmmoPrefab;
    [SerializeField]
    private GameObject fireThrowerPrefab, riffleAmmoPrefab, fireAmmoPrefab, granadePrefab, minePrefab, barrelPrefab, lightPrefab,
        medicalKitPrefab, foodPrefab, armorPrefab, simpleCarPrefab, armoredCarPrefab, compactTankPrefab, allTerrainCarPrefab,
        tankPrefab, radar1Prefab;

    [Header("SHOP CATEGORY BUTTONS")]
    [SerializeField] private Button ammoButton;
    [SerializeField] private Button weaponButton, farmacyButton, carButton, radarButon;
    [Header("SHOP  BUTTONS")]
    [SerializeField] private Button buyButton;
    private const int gunAmmoPrice = 1, riffleAmmoPrice = 2, fireAmmoPrice = 2, granadePrice = 5, minePrice = 5, barrelPrice = 6,
        lightPrice = 10, medicalKitPrice = 4, foodPrice = 10, armorPrice = 15, simpleCarPrice = 25, armoredCarPrice = 40,
        compactTankPrice = 75, allTerrainCarPrice = 100, tankPrice = 150, radar1Price = 10, fireThrowerPrice = 5;

    List<GameObject> displayedItems = new List<GameObject>();
    [SerializeField] private SelectWeaponSystem weaponMenu;

    List<Item> itemsInShop = new List<Item>();

    ItemType selectedItem;
    private ShopCategories actualCategory;

    private PlayerCombatController player;

    private void AddButtonListeners()
    {
        ammoButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Ammo); });
        weaponButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Weapons); });
        farmacyButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Farmacy); });
        carButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.CarShop); });
        radarButon.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Radar); });
    }


    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCombatController>();
        AddButtonListeners();
        AddTemporalItems();
        ChargeShopCategory(ShopCategories.CarShop);
	}
	
    private void AddTemporalItems()
    {
        AddItem(ItemType.FireAmmo, 3);
        AddItem(ItemType.Light, 3);
        AddItem(ItemType.ArmoredCar, 1);
        AddItem(ItemType.Food,1);
        AddItem(ItemType.CompactTank, 1);
        AddItem(ItemType.AllTerrainCar, 1);
        AddItem(ItemType.Armor, 1);
        AddItem(ItemType.Barrel, 3);
        AddItem(ItemType.FireThrower, 1);
        AddItem(ItemType.Granade, 3);
        AddItem(ItemType.GunAmmo, 3);
        AddItem(ItemType.Mine, 3);
    }

    private void AddItem(ItemType weaponType,int amount)
    {
        switch(weaponType)
        {
            case ItemType.AllTerrainCar:
                {
                    AddItemAmount(ItemType.AllTerrainCar, allTerrainCarPrefab, allTerrainCarPrice, amount);
                    return;
                }
            case ItemType.Armor:
                {
                    AddItemAmount(ItemType.Armor, armorPrefab, armorPrice, amount);
                    return;
                }
            case ItemType.ArmoredCar:
                {
                    AddItemAmount(ItemType.ArmoredCar, armoredCarPrefab, armoredCarPrice, amount);
                    return;
                }
            case ItemType.Barrel:
                {
                    AddItemAmount(ItemType.Barrel, barrelPrefab, barrelPrice, amount);
                    return;
                }
            case ItemType.CompactTank:
                {
                    AddItemAmount(ItemType.CompactTank, compactTankPrefab, compactTankPrice, amount);
                    return;
                }
            case ItemType.FireAmmo:
                {
                    AddItemAmount(ItemType.FireAmmo, fireAmmoPrefab, fireAmmoPrice, amount);
                    return;
                }
            case ItemType.FireThrower:
                {
                    AddItemAmount(ItemType.FireThrower, fireThrowerPrefab, fireThrowerPrice, amount);
                    return;
                }
            case ItemType.Food:
                {
                    AddItemAmount(ItemType.Food, foodPrefab, foodPrice, amount);
                    return;
                }
            case ItemType.Granade:
                {
                    AddItemAmount(ItemType.Granade, granadePrefab, granadePrice, amount);
                    return;
                }
            case ItemType.GunAmmo:
                {
                    AddItemAmount(ItemType.GunAmmo, gunAmmoPrefab, gunAmmoPrice, amount);
                    return;
                }
            case ItemType.Light:
                {
                    AddItemAmount(ItemType.Light, lightPrefab, lightPrice, amount);
                    return;
                }
            case ItemType.MedicalKit:
                {
                    AddItemAmount(ItemType.MedicalKit, medicalKitPrefab, medicalKitPrice, amount);
                    return;
                }
            case ItemType.Mine:
                {
                    AddItemAmount(ItemType.Mine, minePrefab, minePrice, amount);
                    return;
                }
            case ItemType.Radar:
                {
                    AddItemAmount(ItemType.Radar, radar1Prefab, radar1Price, amount);

                    return;
                }
            case ItemType.RiffleAmmo:
                {
                    AddItemAmount(ItemType.RiffleAmmo, riffleAmmoPrefab, riffleAmmoPrice, amount);
                    return;
                }
            case ItemType.SimpleCar:
                {
                    AddItemAmount(ItemType.SimpleCar, simpleCarPrefab, simpleCarPrice, amount);
                    return;
                }
            case ItemType.Tank:
                {
                    AddItemAmount(ItemType.Tank, tankPrefab, tankPrice, amount);
                    return;
                }
        }
    }

    private void ChargeShopCategory(ShopCategories category)
    {
        List<Item> itemsToShow = LookItemsInShopByCategory(category);
       
        foreach (GameObject item in displayedItems)
            Destroy(item);
        displayedItems.Clear();

        for (byte i = 0; i<itemsToShow.Count; ++i)
        {
            GameObject instantiatedItem = Instantiate(itemsToShow[i].itemPrefab, shopPositions[i].position, Quaternion.identity);
            instantiatedItem.transform.SetParent(shopPositions[i]);

            ItemType type = itemsToShow[i].itemType;
            instantiatedItem.GetComponent<Button>().onClick.AddListener(delegate { SelectItem(type); });

            instantiatedItem.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = itemsToShow[i].price.ToString();
            instantiatedItem.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = itemsToShow[i].amount.ToString();
            displayedItems.Add(instantiatedItem);
        }
        actualCategory = category;
    }

    private void AddItemAmount(ItemType itemType,GameObject itemPrefab, int itemPrice, int itemAmountToAdd)
    {
        byte i = 0;
        for (; i < itemsInShop.Count && itemsInShop[i].itemType != itemType; ++i)
            ; //null statement        
        if (i == itemsInShop.Count) // if item dont exist in the shop
            itemsInShop.Add(new Item(itemType, itemPrefab, itemPrice, itemAmountToAdd));
        else // if the item already exists in the shop
        {
            Item thisItem = itemsInShop[i];
            thisItem.amount += itemAmountToAdd;
            itemsInShop[i] = thisItem;
        }
    }

    private void SubstractItemAmount(ItemType itemType)
    {
        byte i = 0;
        for (; i < itemsInShop.Count && itemsInShop[i].itemType != itemType; ++i)
            ; //null statement 
             
        if (i != itemsInShop.Count)
        {
            Item thisItem = itemsInShop[i];
            --thisItem.amount;
            if (thisItem.amount <= 0)
            {
                itemsInShop.Remove(itemsInShop[i]);
                ChargeShopCategory(actualCategory);
            }
            else
            {
                itemsInShop[i] = thisItem;
                ChangeDisplayedItemPrice(thisItem.itemPrefab);
            }
        }
        else
            Debug.LogError("Trying substract an item that dont exist in the shop");
    }
        
    private void ChangeDisplayedItemPrice(GameObject itemPrefab)
    {
        byte i = 0;
        for (; i < displayedItems.Count && !displayedItems[i].name.Contains(itemPrefab.name); ++i)
            ;
        int newAmount= Int32.Parse(displayedItems[i].transform.GetChild(0).GetChild(1).GetComponent<Text>().text);
        --newAmount;
        displayedItems[i].transform.GetChild(0).GetChild(1).GetComponent<Text>().text = newAmount.ToString();
    }


    private List<Item> LookItemsInShopByCategory(ShopCategories searchedCategory)
    {
        List<Item> searchedItems= new List<Item>();
        foreach (Item item in itemsInShop)
            if (item.category == searchedCategory)
                searchedItems.Add(item);
            
        return searchedItems;
    }

    private void SelectItem(ItemType itemType)
    {
        selectedItem = itemType;
        buyButton.interactable = true;
    }

    public void BuyItem()
    {
        switch(selectedItem)
        {
            case ItemType.FireThrower:
                weaponMenu.TryAddWeapon(Weapons.FireThrower);
                SubstractItemAmount(ItemType.FireThrower);
                return;
            case ItemType.Granade:
                player.AddAmmo(Weapons.Granade, 7);
                SubstractItemAmount(ItemType.Granade);
                return;
            case ItemType.MedicalKit:
                weaponMenu.TryAddWeapon(Weapons.MedicalKit);
                SubstractItemAmount(ItemType.MedicalKit);
                return;
            case ItemType.Light:
                player.AddAmmo(Weapons.Light, 2);
                SubstractItemAmount(ItemType.Light);
                return;
            case ItemType.GunAmmo:
                player.AddAmmo(Weapons.Gun, 5);
                SubstractItemAmount(ItemType.GunAmmo);
                return;
            case ItemType.FireAmmo:
                player.AddAmmo(Weapons.FireThrower, 5);
                SubstractItemAmount(ItemType.FireAmmo);
                return;
            case ItemType.Barrel:
                player.AddAmmo(Weapons.Barrel, 5);
                SubstractItemAmount(ItemType.Barrel);
                return;
            default:
                throw new NotImplementedException("Bought item still not implemented: " + selectedItem);

        }
    }

    void OnValidate()
    {
        if (shopPositions.Length != shopPositionsLenght)
        {
            Debug.LogWarning("Don't change the 'shopPositions' field's array size!");
            Array.Resize(ref shopPositions, shopPositionsLenght);
        }
    }

    #region ShopItem Struct

    public struct Item
    {
        public ItemType itemType;

        public ShopCategories category;

        public GameObject itemPrefab;

        public int price;
        public int amount;

        private ShopCategories GetCategory()
        {
            if (itemType == ItemType.AllTerrainCar || itemType == ItemType.ArmoredCar || itemType == ItemType.CompactTank ||
                itemType == ItemType.SimpleCar || itemType == ItemType.Tank)
                return ShopCategories.CarShop;
            else if (itemType == ItemType.FireThrower || itemType == ItemType.Barrel || itemType == ItemType.Granade ||
                itemType == ItemType.Mine)
                return ShopCategories.Weapons;
            else if (itemType == ItemType.Armor || itemType == ItemType.Food || itemType == ItemType.MedicalKit)
                return ShopCategories.Farmacy;
            else //if (weaponType == WeaponType.FireAmmo || weaponType == WeaponType.GunAmmo || weaponType == WeaponType.RiffleAmmo)
                return ShopCategories.Ammo;
        }

        public Item(ItemType _weaponType, GameObject _itemPrefab, int _price, int _amount)
        {
            itemType = _weaponType;
            itemPrefab = _itemPrefab;
            price = _price;
            amount = _amount;
            category = 0;
            category = GetCategory();
        }
    }

    #endregion
}
