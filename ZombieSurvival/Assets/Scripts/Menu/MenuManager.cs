﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Script that manage all the open and close input of the menus in the game  </summary>
public class MenuManager : MonoBehaviour
{
    #region Variables

    /// <summary> The SelectWeaponSystem component of the Select Weapon Menu </summary>
    [SerializeField] private SelectWeaponSystem selectorMenu;

    [SerializeField] private ShopSystem shop;

    #endregion

    #region Methods

    void Update ()
    {
        //Input of opening and closing the Menu Weapon Selector 

        if (Input.GetKeyDown(KeyCode.Space))
            selectorMenu.OpenMenu();

        else if (Input.GetKeyUp(KeyCode.Space))
            selectorMenu.CloseMenu();

        else if (Input.GetKeyDown(KeyCode.P))
            shop.Open();

  
    }

    #endregion
}
