﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

//enum ShopItems {GunAmmo, RiffleAmmo, FireAmmo, Granade, Mine, Barrel, Light, MedicalKit, Food, Armor, SimpleCar, ArmoredCar, CompactTank, AllTerrainCar, Tank, Radar1}

public class ShopSystem : MonoBehaviour
{
    private const byte shopPositionsLenght = 5;
    enum AmmoItems { GunAmmo, RiffleAmmo, FireAmmo }
    enum WeaponItems { FireThrower, Granade, Mine, Barrel, Light }
    enum MedicalItems { MedicalKit, Food, Armor }
    enum CarItems { SimpleCar, ArmoredCar, CompactTank, AllTerrainCar, Tank }
    enum RadarItems { Radar }
    enum ShopCategories { Ammo, Weapons, Farmacy, CarShop, Radar }

    private const int gunAmmoPrice = 1, riffleAmmoPrice = 2, fireAmmoPrice = 2, granadePrice = 5, minePrice = 5, barrelPrice = 6,
        lightPrice = 10, medicalKitPrice = 4, foodPrice = 10, armorPrice = 15, simpleCarPrice = 25, armoredCarPrice = 40,
        compactTankPrice = 75, allTerrainCarPrice = 100, tankPrice = 150, radar1Price = 10, fireThrowerPrice=5;

    [SerializeField] private SelectWeaponSystem weaponMenu;
    [SerializeField] private Transform[] shopPositions = new Transform[shopPositionsLenght];

    [Header("SHOP ITEM PREFABS")]
    [SerializeField] private GameObject gunAmmoPrefab;
    [SerializeField]
    private GameObject fireThrower, riffleAmmoPrefab, fireAmmoPrefab, granadePrefab, minePrefab, barrelPrefab, lightPrefab,
        medicalKitPrefab, foodPrefab, armorPrefab, simpeCarPrefab, armoredCarPrefab, compactTankPrefab, allTerrainCarPrefab,
        tankPrefab, radar1Prefab;

    //m_YourSecondButton.onClick.AddListener(delegate {TaskWithParameters("Hello");
    [Header("SHOP BUTTONS")]
    [SerializeField] private Button ammoButton;
    [SerializeField] private Button weaponButton, farmacyButton, carButton, radarButon;


    Dictionary<AmmoItems, ItemTexts> ammoItems = new Dictionary<AmmoItems, ItemTexts>();
    Dictionary<WeaponItems, ItemTexts> weaponItems = new Dictionary<WeaponItems, ItemTexts>();
    Dictionary<MedicalItems, ItemTexts> medicalItems = new Dictionary<MedicalItems, ItemTexts>();
    Dictionary<CarItems, ItemTexts> carItems = new Dictionary<CarItems, ItemTexts>();
    Dictionary<RadarItems, ItemTexts> radarItems = new Dictionary<RadarItems, ItemTexts>();

    private byte[] numberOfItems;
    List<GameObject> displayedItems = new List<GameObject>();

    private void Start()
    {
        AddButtonListeners();
        AddTempItems();

        ChargeShopCategory(ShopCategories.Ammo);
    }

    private void AddTempItems()
    {
        AddAmmoItem(AmmoItems.FireAmmo, 50);
        AddAmmoItem(AmmoItems.GunAmmo, 60);
        AddAmmoItem(AmmoItems.RiffleAmmo, 10);

        AddWeaponItem(WeaponItems.FireThrower, 1);
        AddWeaponItem(WeaponItems.Barrel, 10);
        AddWeaponItem(WeaponItems.Granade, 22);
        AddWeaponItem(WeaponItems.Light, 13);
        AddWeaponItem(WeaponItems.Mine, 5);

        AddMedicalItem(MedicalItems.Armor, 2);
        AddMedicalItem(MedicalItems.Food, 1);
        AddMedicalItem(MedicalItems.MedicalKit, 3);

        AddCarItem(CarItems.AllTerrainCar, 1);
        AddCarItem(CarItems.ArmoredCar, 1);
        AddCarItem(CarItems.CompactTank, 1);
        AddCarItem(CarItems.SimpleCar, 1);
        AddCarItem(CarItems.Tank, 1);
    }


    private void AddButtonListeners()
    {
        ammoButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Ammo); });
        weaponButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Weapons); });
        farmacyButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Farmacy); });
        carButton.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.CarShop); });
        radarButon.onClick.AddListener(delegate { ChargeShopCategory(ShopCategories.Radar); });
    }

    private void AddAmmoItem(AmmoItems item, int amount)
    {
        int price = 0;

        switch (item)
        {
            case AmmoItems.FireAmmo:
                price = fireAmmoPrice;
                break;
            case AmmoItems.GunAmmo:
                price = gunAmmoPrice;
                break;
            default: //case RiffleAmmo:
                price = riffleAmmoPrice;
                break;
        }

        ammoItems.Add(item, new ItemTexts(price, amount));
    }

    private void AddMedicalItem(MedicalItems item, int amount)
    {
        int price = 0;
        switch (item)
        {
            case MedicalItems.Armor:
                {
                    price = armorPrice;
                    break;
                }
            case MedicalItems.Food:
                {
                    price = foodPrice;
                    break;
                }
            default: // medical kit
                {
                    price = medicalKitPrice;
                    break;
                }
        }

        medicalItems.Add(item, new ItemTexts(price, amount));
    }

    private void AddWeaponItem(WeaponItems item, int amount)
    {
        int price = 0;
        switch (item)
        {
            case WeaponItems.Barrel:
                {
                    price = barrelPrice;
                    break;
                }
            case WeaponItems.Granade:
                {
                    price = granadePrice;
                    break;
                }
            case WeaponItems.Light:
                {
                    price = lightPrice;
                    break;
                }
            case WeaponItems.Mine: 
                {
                    price = minePrice;
                    break;
                }
            default: // case Firethrower
                {
                    price = fireThrowerPrice;
                    break;
                }
                
        }
        Debug.Log("item:" + item);
        weaponItems.Add(item, new ItemTexts(price, amount));
    }

    private void AddCarItem(CarItems item, int amount)
    {
        int price = 0;
        switch (item)
        {
            case CarItems.AllTerrainCar:
                {
                    price = allTerrainCarPrice;
                    break;
                }
            case CarItems.ArmoredCar:
                {
                    price = armoredCarPrice;
                    break;
                }
            case CarItems.CompactTank:
                {
                    price = compactTankPrice;
                    break;
                }
            case CarItems.SimpleCar:
                {
                    price = simpleCarPrice;
                    break;
                }
            default: // case Tank
                {
                    price = tankPrice;
                    break;
                }
        }

        carItems.Add(item, new ItemTexts(price, amount));
    }

    /*private void AddRadarItemTemp(RadarItems item)
    {
        radarItems.Add(item);
    }*/

    private void OpenShop()
    {

    }

    private void ChargeShopCategory(ShopCategories category)
    {
        List<GameObject> prefabList = new List<GameObject>();
        List<ItemTexts> itemTexts = new List<ItemTexts>();
        foreach (GameObject item in displayedItems)
            Destroy(item);
        prefabList.Clear();

        switch (category)
        {
            case ShopCategories.Ammo:
                {
                    if (ammoItems.ContainsKey(AmmoItems.FireAmmo))
                    {
                        prefabList.Add(fireAmmoPrefab);
                        itemTexts.Add(ammoItems[AmmoItems.FireAmmo]);
                    }
                    if (ammoItems.ContainsKey(AmmoItems.GunAmmo))
                    {
                        prefabList.Add(gunAmmoPrefab);
                        itemTexts.Add(ammoItems[AmmoItems.GunAmmo]);
                    }
                    if (ammoItems.ContainsKey(AmmoItems.RiffleAmmo))
                    {
                        prefabList.Add(riffleAmmoPrefab);
                        itemTexts.Add(ammoItems[AmmoItems.RiffleAmmo]);

                    }
                    break;
                }
            case ShopCategories.CarShop:
                {
                    if (carItems.ContainsKey(CarItems.AllTerrainCar))
                    {
                        prefabList.Add(allTerrainCarPrefab);
                        itemTexts.Add(carItems[CarItems.AllTerrainCar]);
                    }
                    if (carItems.ContainsKey(CarItems.ArmoredCar))
                    {
                        prefabList.Add(armoredCarPrefab);
                        itemTexts.Add(carItems[CarItems.ArmoredCar]);

                    }
                    if (carItems.ContainsKey(CarItems.CompactTank))
                    {
                        prefabList.Add(compactTankPrefab);
                        itemTexts.Add(carItems[CarItems.CompactTank]);
                    }
                    if (carItems.ContainsKey(CarItems.SimpleCar))
                    {
                        prefabList.Add(simpeCarPrefab);
                        itemTexts.Add(carItems[CarItems.SimpleCar]);
                    }
                    if (carItems.ContainsKey(CarItems.Tank))
                    {
                        prefabList.Add(tankPrefab);
                        itemTexts.Add(carItems[CarItems.Tank]);
                    }

                    break;
                }


            case ShopCategories.Farmacy:
                {
                    if (medicalItems.ContainsKey(MedicalItems.Armor))
                    {
                        prefabList.Add(armorPrefab);
                        itemTexts.Add(medicalItems[MedicalItems.Armor]);

                    }
                    if (medicalItems.ContainsKey(MedicalItems.Food))
                    {
                        prefabList.Add(foodPrefab);
                        itemTexts.Add(medicalItems[MedicalItems.Food]);
                    }
                    if (medicalItems.ContainsKey(MedicalItems.MedicalKit))
                    {
                        prefabList.Add(medicalKitPrefab);
                        itemTexts.Add(medicalItems[MedicalItems.MedicalKit]);
                    }
                    break;
                }
            case ShopCategories.Radar:
                {
                    if (radarItems.ContainsKey(RadarItems.Radar))
                    {
                        prefabList.Add(radar1Prefab);
                        itemTexts.Add(radarItems[RadarItems.Radar]);
                    }
                    break;
                }
            case ShopCategories.Weapons:
                {
                    if (weaponItems.ContainsKey(WeaponItems.Barrel))
                    {
                        prefabList.Add(barrelPrefab);
                        itemTexts.Add(weaponItems[WeaponItems.Barrel]);
                    }
                    if (weaponItems.ContainsKey(WeaponItems.Granade))
                    {
                        prefabList.Add(granadePrefab);
                        itemTexts.Add(weaponItems[WeaponItems.Granade]);
                    }
                    if (weaponItems.ContainsKey(WeaponItems.Light))
                    {
                        prefabList.Add(lightPrefab);
                        itemTexts.Add(weaponItems[WeaponItems.Light]);
                    }
                    if (weaponItems.ContainsKey(WeaponItems.Mine))
                    {
                        prefabList.Add(minePrefab);
                        itemTexts.Add(weaponItems[WeaponItems.Mine]);
                    }
                    if (weaponItems.ContainsKey(WeaponItems.FireThrower))
                    {
                        prefabList.Add(fireThrower);
                        itemTexts.Add(weaponItems[WeaponItems.FireThrower]);
                    }
                    break;
                }
        }

        for (byte i = 0; i < prefabList.Count; ++i)
        {
            GameObject item = Instantiate(prefabList[i], shopPositions[i].position, prefabList[i].transform.rotation);
            item.GetComponent<Button>().onClick.AddListener(delegate { BuyItem(item.transform.GetChild(0).name); });
            
            item.transform.SetParent(shopPositions[i]);
            item.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = itemTexts[i].price.ToString();
            item.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = itemTexts[i].amount.ToString();
            displayedItems.Add(item);
        }
    }

    void OnValidate()
    {
        if (shopPositions.Length != shopPositionsLenght)
        {
            Debug.LogWarning("Don't change the 'shopPositions' field's array size!");
            Array.Resize(ref shopPositions, shopPositionsLenght);
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        GetComponent<Animator>().SetTrigger("Close");
    }

    public void DisableGameoject()
    {
        gameObject.SetActive(false);
    }

    public void BuyItem(string typeName)
    {
        Debug.Log("hii: " + typeName);
        switch (typeName)
        {
            case "fireThrower":
               
                weaponMenu.TryAddWeapon(Weapons.FireThrower);
                break;
        }
    }



}




public struct ItemTexts
{
    public int price, amount;

    public ItemTexts(int _price, int _amount)
    {
        price = _price;
        amount = _amount;
    }
}