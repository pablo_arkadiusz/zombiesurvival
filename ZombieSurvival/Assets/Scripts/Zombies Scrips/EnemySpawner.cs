﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Script that manages the creation of enemies </summary>
public class EnemySpawner : MonoBehaviour
{
    #region Variables

    /// <summary> Array with all the positions where the enemies spawns </summary>
    [Tooltip(" Array with all the positions where the enemies spawns ")]
    [SerializeField] private Transform[] spawningPos;
    /// <summary> The prefab of the zombie that will be instantiated </summary>
    [Tooltip(" The prefab of the zombie that will be instantiated ")]
    [SerializeField] private GameObject zombiePrefab;
    /// <summary> The Gameobject where the zombies are organized </summary>
    [Tooltip(" The Gameobject where the zombies are organized ")]
    [SerializeField] private Transform enemiesParent;

    /// <summary> Index to go through all the spawning positions </summary>
    private byte spawningPosIndex=0;
    /// <summary> Property to manage the spawningPos index limit </summary>
    private byte SpawningPosIndex{ set { if (value > spawningPos.Length - 1) spawningPosIndex = 0; else spawningPosIndex = value; } get { return spawningPosIndex; } }

    #endregion

    #region Methods

    /// <summary> Spawns all enemies changing between the spawning position every spawn </summary>
    private IEnumerator SpawnEnemies()
    {
        int enemiesToSpawn = 100;
        for(byte i=0;i<enemiesToSpawn;++i,++SpawningPosIndex)
        {
            Instantiate(zombiePrefab, spawningPos[SpawningPosIndex].position, Quaternion.identity).transform.parent = enemiesParent;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    #endregion
}
