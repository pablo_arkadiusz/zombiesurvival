﻿using UnityEngine;
using System.Collections;

/// <summary> Zombie IA script. It has two diffent states: Following the path and Attack the player </summary>
public class Zombie : MonoBehaviour
{
    #region Variables

    //CONST

    /// <summary> Circunference around targets to reached them earlier and rotate more smoothly to the next one. Visible on gizmos </summary>
    private const float reachArea=0.3f;
    /// <summary> Distance needed to reach a player </summary>
    private const float reachAreaToPlayer = 6f;
    /// <summary> Layer where the zombie is going after dying </summary>
    private const byte zombieDeadLayer = 15;

   

    //ZOMBIE STATS

    /// <summary> The life points that the zombie will take from the player </summary>
    [Header("Zombie stats")] [Tooltip("The life points that the zombie will take from the player")]
    [SerializeField] private float damage;
    /// <summary> Health points of the zombie </summary>
    [Tooltip("Health points of the zombie")]
    [SerializeField] private float health = 100;
    /// <summary> Movement speed of zombie </summary>
    [Tooltip("Movement speed of zombie")]
    [SerializeField] private float speed = 5;
    /// <summary> The range of the zombie. Visible on gizmos </summary>
    [Tooltip("The range of the zombie. Visible on gizmos")]
    [SerializeField] private float attackArea;

    //LAYERS

    /// <summary> the layer whose elements the zombie causes damage </summary>
    [Header("Layer")][SerializeField] private LayerMask targetLayer;

    //MATERIALS

    //The shadow needs a new material when zombie die because it needs to decrease
    //his opacity and the custom stencil material cant modify it .
    /// <summary> The material of the shadow when player die. </summary>
    [Header("Material")] [Tooltip("The material of the shadow when player die.")]
    [SerializeField] private Material shadowDieMaterial;



    //PRIVATE/NON SERIALIZABLE

    /// <summary> Target's Transform component. It can be player, npc or a car that player is using </summary>
    public static Transform target;
    /// <summary> Array with A-Star Path that this zombie follows, it get's updated every X time, to point to player's position </summary>
    private Vector3[] path;
    /// <summary>Index of the path that this object is following</summary>
    private int targetIndex;
    /// <summary> Rigidbody2D of this object </summary>
    private Rigidbody2D rb;
    /// <summary> ZombieAnimatorController Component of this object </summary>
    private ZombieAnimatorController animatorController;
    /// <summary> Zombie state </summary>
    private bool attacking, walking = true;
    /// <summary > Is the attack on cooldown ? </summary>
    private bool attackCooldown;
    /// <summary > Cooldown time between melee attacks </summary>
    private float cooldownTime;
    /// <summary > Coroutine of following path, to only have one following path funcion at time </summary>
    private Coroutine followingPath;
    /// <summary > Is the zombie dead ? </summary>
    private bool died;

    #endregion

    #region Methods

    /// <summary > Function that controls the zombie IA. Updates the zombie actions every X time based on the zombie state (walking or attacking) </summary>
    private IEnumerator ControlZombieLogic()
    {
        while(true)
        {
            ControlState();
            yield return new WaitForSeconds(0.75f);
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animatorController = GetComponent<ZombieAnimatorController>();
        target = ZombieTarget.target;
        StartCoroutine(ControlZombieLogic());
    }

    /// <summary >  Updates the zombie actions based on the zombie state(walking or attacking) </summary>
    private void ControlState()
    {
        //If the zombie is walking it request to the PathRequestManager to follow a new path. The manager will update his queue, 
        //finds a new path and return the petition calling the function OnPathFound as a callback
        if (walking)
            PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));

        else if (attacking && !attackCooldown)
            Attack();
    }

    /// <summary > Function used as a callback from PathRequestManager when the path is found </summary>
    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (walking && pathSuccessful)
        {
            //The zombie starts the walk animation
            animatorController.StartWalkAnimation();

            //Path to follow is updated to point at player position
            path = newPath;
            targetIndex = 0;

            //Starts following the path
            if (followingPath != null)
                StopCoroutine(followingPath);
            if(!died)
                followingPath = StartCoroutine(FollowPath());
        }
    }

    /// <summary > Charges the cooldown of the melee attack </summary>
    private IEnumerator ChargeCooldown()
    {
        yield return new WaitForSeconds(1);
        attackCooldown = false;
    }

    /// <summary > Start the attack animation and starts the cooldown </summary>
    private void Attack()
    {
        attackCooldown = true;
        StartCoroutine(ChargeCooldown());
        animatorController.StartAttackAnimation();
    }

    /// <summary > Function called from Zombie Attack Animation. Checks if the player is still close. If it isnt it go back to the walking state </summary>
    public void CheckIfIsCloseToTarget()
    {
        if(Vector2.Distance(transform.position, target.position) > reachAreaToPlayer)
        {
            attacking = false;
            walking = true;
        }
    }

    /// <summary > Follow the path created in the PathFinding </summary>
    private IEnumerator FollowPath()
    {
        Vector3 currentWaypoint;

        if (path.Length>0) //sometimes the path dont have waypoints. When the zombie is actually already on player position and just started to attack
            currentWaypoint = path[0];
        else yield break;

        // As its a 2D game ZPos will remains the same
        float zPos = transform.position.z;

        while (true)
        {
            //If the "currentWaypoint" it isnt the last path-point before the player/target and if the zombie is close enough to the currentWaypoint
            if (targetIndex < path.Length - 1 && Vector2.Distance(transform.position, currentWaypoint) <= reachArea)
            {
                //update the "currentWaypoint" to the next one
                targetIndex++;
                currentWaypoint = path[targetIndex];
            }
            //If the "currentWaypoint" it's the last path-point before the player/target and if the zombie is close enough to the currentWaypoint
            else if (targetIndex >= path.Length - 1 && Vector2.Distance(transform.position, target.position) <= reachAreaToPlayer)
            {
                //start the attack state
                walking = false;
                attacking = true;
                yield break;
            }
            currentWaypoint.z = zPos;

            //Direction the zombie will face
            Vector2 direction = (currentWaypoint - transform.position).normalized;

            //Move forwards (based on the rotation direction)
            rb.MovePosition(transform.position + -transform.up * speed * Time.deltaTime);

            //Rotate slowly to the position of currentWaypoint/target
            float rotationZ = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, 0.0f, rotationZ + 90), speed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary > Function called from the attack animaion. It do damage to the player if he is in the "attackArea" </summary>
    public void DoDamageInArea()
    {
        Collider2D targetCollider = Physics2D.OverlapCircle(transform.position, attackArea, targetLayer);
        if (targetCollider)
        {
            PlayerCombatController player = targetCollider.GetComponent<PlayerCombatController>();
            if(player)
               player.TakeDamage(damage);
            else
                targetCollider.GetComponent<CarLife>().TakeDamage(damage);
            

        }
    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            // Draws the zombie's path in the gizmos 
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);
                Gizmos.DrawWireSphere(path[i], reachArea);
                if (i == targetIndex)
                    Gizmos.DrawLine(transform.position, path[i]);
                else
                    Gizmos.DrawLine(path[i - 1], path[i]);
            }
        }
        // Draws the attack area
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackArea);
    }

    /// <summary > Disable the zombie </summary>
    private void DeactivateZombie()
    {
        died = true;
        gameObject.layer = zombieDeadLayer;
        
        transform.GetChild(1).GetComponent<SpriteRenderer>().material = shadowDieMaterial;
        StopAllCoroutines();
        enabled = false;
    }

    //TODO: comprobar si esta implementado
    /// <summary > Called from animator during the die animation to make the zombie be in less Z position than the alive zombies </summary>
    public void GoToBackground()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.1f);
    }

    /// <summary> Zombie lose life. Called from player attack functions, bullets and all things that make zombie lose life </summary>
    public void TakeDamage(float damage)
    {
        if (health <= 0) return;
        health -= damage;
        if (health <= 0)
        {
            animatorController.PlayDieAnimation();
            DeactivateZombie();
        }
    }
    #endregion
}

public static class ZombieTarget
{
    static public Transform target;
    static public Transform reachPos;
}
