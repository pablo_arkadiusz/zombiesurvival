﻿using UnityEngine;

/// <summary> Script that manages zombie animations and also the animations of zombie's black sprites and shadow </summary>
public class ZombieAnimatorController : MonoBehaviour
{
    /* The zombie has a black sprites that player can see in his radar distance, this sprites dissapears if the light raycast touch them (FieldOfView.cs)
    Also the zombie has a shadow that follows him but is only visible when the light is when the FieldOfView is on. Because his material is only visible 
    in that shader This script also manages that two sprites animation */

    #region Variables

    /// <summary> The main animator of the zombie </summary>
    private Animator myAnimator;
    /// <summary> The animator of the black sprites of the zombie </summary>
    private Animator blackAnimator;
    /// <summary> The animator of the zombie´s shadow </summary>
    private Animator shadowAnimator;

    #endregion

    #region Methods

    private void Start()
    {
        myAnimator = GetComponent(typeof(Animator)) as Animator;
        blackAnimator = transform.GetChild(0).GetComponent(typeof(Animator)) as Animator;
        shadowAnimator = transform.GetChild(1).GetComponent(typeof(Animator)) as Animator;
    }

    /// <summary> Sets the zombie sprites, black sprites and shadows animations to attack </summary>
    public void StartAttackAnimation()
    {
        StopWalkAnimation();
        myAnimator.SetTrigger("Attack");
        blackAnimator.SetTrigger("Attack");
        shadowAnimator.SetTrigger("Attack");
    }

    /// <summary> Sets the zombie sprites, black sprites and shadows animations to walk </summary>
    public void StartWalkAnimation()
    {
        myAnimator.SetBool("Walking",true);
        blackAnimator.SetBool("Walking", true);
        shadowAnimator.SetBool("Walking", true);
    }

    /// <summary> Sets the zombie sprites, black sprites and shadows animations to stop walking </summary>
    public void StopWalkAnimation()
    {
        myAnimator.SetBool("Walking", false);
        blackAnimator.SetBool("Walking", false);
        shadowAnimator.SetBool("Walking", false);
    }

    /// <summary> Sets the zombie sprites, black sprites and shadows animations to die </summary>
    public void PlayDieAnimation()
    {
        myAnimator.SetTrigger("Die");
        blackAnimator.SetTrigger("Die");
        shadowAnimator.SetTrigger("Die");
    }
    
    #endregion
}
