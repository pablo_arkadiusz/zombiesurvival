﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour
{
    /// <summary> Movement speed of the Light </summary>
    [Tooltip(" Movement speed of the Light ")]
    [SerializeField] private float moveSpeed = 10f;

    [HideInInspector] public Vector2 finalPosition = Vector2.zero;

    [HideInInspector] public Transform target;
    [HideInInspector] public bool isThrowed;

    private void Start()
    {
        StartCoroutine(FollowPlayer());
    }


    private IEnumerator FollowPlayer()
    {
        while (!isThrowed)
        {
            transform.position = target.position;
            yield return new WaitForEndOfFrame();
        }
        StartCoroutine(TranslateToFinalPosition());
    }

    
    private IEnumerator TranslateToFinalPosition()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        while(Vector2.Distance(transform.position, finalPosition) >= 3)
        {
            transform.Translate(transform.right * moveSpeed * Time.deltaTime, Space.World);
            yield return new WaitForEndOfFrame();
        }
    }

    public void SetRotation(Vector3 rotation) { transform.eulerAngles = rotation; }

    // Move the bullet in the left direction (is the forward site of the sprite)
    private void Update()
    {
        if(finalPosition!=Vector2.zero)
        Debug.Log("hi: " + (Vector2.Distance(transform.position, finalPosition)));
    }
}
