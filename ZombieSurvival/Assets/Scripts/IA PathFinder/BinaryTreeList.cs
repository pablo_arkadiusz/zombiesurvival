﻿using System;

/// <summary> Customice List class to use for Node Storing </summary>
//Just to optimice code. With this the node finding system runs much faster than with a normal List
//Actually is used to store the open Nodes in the find path method (PathFinding.cs)
public class BinaryTreeList<T> where T : IHeapItem<T>
{
    T[] items;
    int currentItemCount;

    public BinaryTreeList(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    public void Add(T item)
    {
        item.BinaryTreeIndex = currentItemCount;
        items[currentItemCount] = item;
        SortUp(item);
        currentItemCount++;
    }

    public T RemoveFirst()
    {
        T firstItem = items[0];
        currentItemCount--;
        items[0] = items[currentItemCount];
        items[0].BinaryTreeIndex = 0;
        SortDown(items[0]);
        return firstItem;
    }

    public void UpdateItem(T item)
    {
        SortUp(item);
    }

    public int Count
    {
        get
        {
            return currentItemCount;
        }
    }

    public bool Contains(T item)
    {
        return Equals(items[item.BinaryTreeIndex], item);
    }

    void SortDown(T item)
    {
        while (true)
        {
            int childIndexLeft = item.BinaryTreeIndex * 2 + 1;
            int childIndexRight = item.BinaryTreeIndex * 2 + 2;
            int swapIndex = 0;

            if (childIndexLeft < currentItemCount)
            {
                swapIndex = childIndexLeft;

                if (childIndexRight < currentItemCount)
                {
                    if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                {
                    return;
                }

            }
            else
            {
                return;
            }

        }
    }

    void SortUp(T item)
    {
        int parentIndex = (item.BinaryTreeIndex - 1) / 2;

        while (true)
        {
            T parentItem = items[parentIndex];
            if (item.CompareTo(parentItem) > 0)
            {
                Swap(item, parentItem);
            }
            else
            {
                break;
            }

            parentIndex = (item.BinaryTreeIndex - 1) / 2;
        }
    }

    void Swap(T itemA, T itemB)
    {
        items[itemA.BinaryTreeIndex] = itemB;
        items[itemB.BinaryTreeIndex] = itemA;
        int itemAIndex = itemA.BinaryTreeIndex;
        itemA.BinaryTreeIndex = itemB.BinaryTreeIndex;
        itemB.BinaryTreeIndex = itemAIndex;
    }
}

public interface IHeapItem<T> : IComparable<T>
{
    int BinaryTreeIndex
    {
        get;
        set;
    }
}