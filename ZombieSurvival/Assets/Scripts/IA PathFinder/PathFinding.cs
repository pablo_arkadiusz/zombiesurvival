﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Diagnostics; To measure the time that it takes to find the path

/// <summary> Class that uses "A-star Algorithm" to find the best path to a target position avoiding all the obstacles </summary>
public class PathFinding : MonoBehaviour
{
    /// <summary> Component that creates the grid and all the nodes </summary>
    private NodesGrid grid;

    private void Awake(){grid = GetComponent<NodesGrid>(); }

    #region Main PathFinding Method

    /// <summary> Uses the "A-star Algorithm" to find the shortest path from actual position to the target position evading the obstacles </summary>
    public void FindPath(PathRequest request, Action<PathResult> NotifyFinishedPath_Callback)
    {
        //Stopwatch sw = new Stopwatch(); To measure the time it takes to find the path
        //sw.Start();

        //The waypoints that forms the path to the target point
        Vector3[] waypoints = new Vector3[0];

        //To see wheter or not the path findig was a succes
        bool pathSucces = false;

        //Gets the starting and ending node
        Node startNode = grid.FindNodeFromWorldPoint(request.pathStart);
        Node targetNode = grid.FindNodeFromWorldPoint(request.pathEnd);

        if (targetNode.walkable)
        {
            //OPEN NODES: Nodes that are near to the actual node but still hasnt been evaluated
            BinaryTreeList<Node> openNodes = new BinaryTreeList<Node>(grid.MaxSize);
            //CLOSED NODES: Nodes that has already been evaluated
            HashSet<Node> closedNodes = new HashSet<Node>();

            openNodes.Add(startNode);

            //loop that end when the target has been found
            while (openNodes.Count > 0)
            {
                //Remove the first node of the BinaryTree and add it to the closed list. Because its gonna be evaluated
                Node currentNode = openNodes.RemoveFirst();
                closedNodes.Add(currentNode);

                //If the target node has been found, the function create the path and breaks from the loop
                if (currentNode == targetNode)
                {
                    //sw.Stop();
                    //print(" path found: " + sw.ElapsedMilliseconds + "ms");
                    pathSucces = true;
                    RetracePath(startNode, targetNode);
                    break;
                }

                //Loop that goes through all the nodes that are near of the current node (neighbours)
                foreach (Node neighbour in grid.GetNeighbours(currentNode))
                {
                    if (!neighbour.walkable || closedNodes.Contains(neighbour))
                        continue; //Dont evaluate the closed Nodes and the not walkables

                    int newMoveCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) /*+ neighbour.movementPenalty*/;

                    //Update the cost to travel to neighbour to find the shortest path to the target
                    //only if it is shorter than the prevoius one or if itsnt already open node     
                    if (newMoveCostToNeighbour < neighbour.gCost || !openNodes.Contains(neighbour))
                    {
                        neighbour.gCost = newMoveCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);

                        //Stores the node as parent. So it will be possible to travel going from parent to parent
                        neighbour.parent = currentNode;

                        //if it wasnt already open node it opens it
                        if (!openNodes.Contains(neighbour))
                            openNodes.Add(neighbour);
                        else
                            openNodes.UpdateItem(neighbour);
                    }
                }
            }
        }
        //Retrace the path if the path succeded
        if (pathSucces)
            waypoints = RetracePath(startNode, targetNode);
        NotifyFinishedPath_Callback(new PathResult(waypoints, pathSucces, request.callback));
    }

    #endregion

    #region Auxiliar Methods

    /// <summary> Create the path from the start point to the target point </summary>
    private Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        //Travel from the end node to the start node. Following the path that has been setted through the parent of the nodes
        while (currentNode!= startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        return waypoints;
    }

    /// <summary> Just erase all unnecessary intermediate points in the path </summary>
    private Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 direcionOld = Vector2.zero;

        for(int i=1;i<path.Count;++i)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if(directionNew!=direcionOld)
                waypoints.Add(path[i].position);
            direcionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    /// <summary> Get a virtual distance from node A to nodeB. Moving one position horizontally
    /// and vertically cost 10units and moving one unit diagonally costs 14 </summary>
    private int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        //Actually the formula to calculate the distance is : 14 * shortest coordinate + 10 * (longest coordinate - shortest coordinate)
        return (distX > distY) ? 
             4 * distY + 10 * distX:
             4 * distX + 10 * distY;
    }

    #endregion
}
