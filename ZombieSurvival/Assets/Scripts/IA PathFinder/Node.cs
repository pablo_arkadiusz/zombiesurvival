﻿using UnityEngine;

/// <summary> Node class. Represent the objects that forms the grid. It is used to be the points used in the "A-start Algorithm" </summary>
public class Node : IHeapItem<Node>  //Implements the IHeapItem to be used in the BinaryTreeList
{
    #region Variables

    /// <summary> Is this node a walkable zone or an obstacle ? </summary>
    public bool walkable;

    /// <summary> World position of the node </summary>
    public Vector2 position;

    /// <summary> The index of this node in the grid </summary>
    public int gridX, gridY;

    //Variables needed to implement the "A-start Algorithm"
    /// <summary> gCost is the distance of the actual node from the startNode </summary>
    public int gCost;
    /// <summary> hCost is the distance of the actual node from the targetNode </summary>
    public int hCost;
    /// <summary> fCost is the sum of gCost and hCost </summary>
    public int FCost { get { return gCost + hCost; } }

    /// <summary> The next node to travel when following the final path </summary>
    public Node parent;

    /// <summary> Movement penalty of the region where this node is. There are different types of regions, with different priorities
    /// It's used to make the enemy path dont go too close to avoid the rubbing/friction with objects</summary>
    public int movementPenalty;

    #endregion

    #region Constructor

    public Node(bool _walkable, Vector2 _position, int _gridX,int _gridY, int _movementPenalty)
    {
        walkable = _walkable;
        position = _position;
        gridX = _gridX;
        gridY = _gridY;
        movementPenalty = _movementPenalty;
    }

    #endregion

    #region IHeapItem interface variables and methods

    int binaryTreeIndex;
    public int BinaryTreeIndex
    {
        get{return binaryTreeIndex;}
        set{binaryTreeIndex = value;}
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = FCost.CompareTo(nodeToCompare.FCost);
        if (compare == 0)
            compare = hCost.CompareTo(nodeToCompare.hCost);
        return -compare;
    }

    #endregion
}
