﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Collections;

/// <summary> Script that manages the path creation of the zombie enemies. It use a threading and queue system </summary>
public class PathRequestManager : MonoBehaviour //Used to be more efficient and not create all the paths of enemy at once
{
    #region Variables

    /// <summary> Queue to dont create all paths of enemies at once, because it would cost a lot of resources </summary>
    Queue<PathResult> results = new Queue<PathResult>();

    /// <summary> Instance/Singleton of PathRequestManager to call static methods </summary>
    static PathRequestManager instance;

    /// <summary> PathFinding component </summary>
    PathFinding pathFinding;

    #endregion

    #region Methods

    void Awake()
    {
        instance = this;
        pathFinding = GetComponent<PathFinding>();
        StartCoroutine(Updt());
    }

    private void Update()
    {
        
    }

    private IEnumerator Updt()
    {
        while(true)
        {
            if (results.Count > 0)
            {
                int itemsInQueue = results.Count;
                lock (results) //only one Thread at time can acces it
                {
                    for (int i = 0; i < itemsInQueue; ++i) //Invoque the callback methods that are in a queue of the enemies trying to find a path
                    {
                        PathResult result = results.Dequeue();
                        result.callback(result.path, result.succes);
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }


    /// <summary> Start processing the path using threading to process multiple enemy paths at time </summary>
    public static void RequestPath(PathRequest request)
    {
        ThreadStart threadStart = delegate
        {
            instance.pathFinding.FindPath(request, instance.FinishedProcessingPath);
        };
        threadStart.Invoke();
    }

    /// <summary> Notify that a path process has been finished. used as a callback when the path creation is completed </summary>
    private void FinishedProcessingPath(PathResult result) //It is used at the end of PathFinding when the path is created
    {
        lock (results) //only one Thread at time can acces it
        {
            results.Enqueue(result);
        }
    }
}

/// <summary> Struct that represents the paths </summary>
public struct PathResult
{
    public Vector3[] path;
    public bool succes;
    public Action<Vector3[], bool> callback;

    //Contructor
    public PathResult(Vector3[] path, bool succes, Action<Vector3[], bool> callback)
    {
        this.path = path;
        this.succes = succes;
        this.callback = callback;
    }

    #endregion
}
#region Structs to organice variables

/// <summary> Struct that represents the paths-request (the start and end point and a callback method)</summary>
public struct PathRequest
{
    public Vector3 pathStart;
    public Vector3 pathEnd;
    public Action<Vector3[], bool> callback;//Action to do when the path is processed. For example the enemy start following the path

    //Contructor
    public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback)
    {
        pathStart = _start;
        pathEnd = _end;
        callback = _callback;
    }

    #endregion
}