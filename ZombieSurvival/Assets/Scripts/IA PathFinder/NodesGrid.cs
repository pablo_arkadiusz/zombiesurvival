﻿using System.Collections.Generic;
using UnityEngine;

/// <summary> Class that manages the node-grid operations like creation the grid with all nodes, find nodes, etc  </summary>
public class NodesGrid : MonoBehaviour
{
    #region Variables

    /// <summary> If true it show the grid and all the nodes in the Gizmos when the game starts </summary>
    [Tooltip("If true it show the grid and all the nodes in the Gizmos when the game starts")]
    [SerializeField] private bool displayGridGizmos = true;

    /// <summary> Player transform, just to show the player position node in Gizmos in diferent color </summary>
    [Tooltip("Player transform, just to show the player position node in Gizmos in diferent color")]
    [SerializeField] private Transform player;

    /// <summary> Layers that enemies cant walk through </summary>
    [Tooltip("Layers that enemies cant walk through")]
    [SerializeField] private LayerMask unwalkableMask;

    /// <summary> Area of the world that the gris is gonna cover </summary>
    [Tooltip("Area of the world that the gris is gonna cover")]
    [SerializeField] private Vector2 gridWorldSize;

    /// <summary> The radius that each individual node covers </summary>
    [Tooltip("The radius that each individual node covers")]
    [SerializeField] private float nodeRadius;

    /// <summary> WalkableRegions with different terrotory prioryty. Only should have selected one Layer per every region </summary>
    [Tooltip("WalkableRegions with different terrotory prioryty. Only should have selected one Layer per every region")]
    [SerializeField] private TerrainType[] walkableRegions;

    /// <summary> Dictionary to store all walkable regions, used to get the movement penalty giving the layer(key) </summary>  
    private Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int>();

    /// <summary> Two dimensional array of the nodes that represent the grid </summary>
    private Node[,] grid;

    /// <summary> Diameter of each node that are part of the grid </summary>
    private float nodeDiameter;

    /// <summary> Number of nodes in the specific coordinate </summary>
    private int nodesNumX, nodesNumY;

    #endregion

    #region Methods

    private void Awake()
    {
        //Set the node values
        nodeDiameter = nodeRadius * 2;
        nodesNumX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        nodesNumY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        //Store all the walkableRegions in the walkable dictionary
        foreach(TerrainType region in walkableRegions)
            walkableRegionsDictionary.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainPenalty);
        
        CreateGrid();
    }

    /// <summary> Get the number of nodes in the grid </summary>
    public int MaxSize{get{ return nodesNumX * nodesNumY; }} //Used to set the size of the BinaryTreeList where the nodes are stored in the PathFinding.cs

    /// <summary> Create a grid of nodes </summary>
    private void CreateGrid()
    {
        //Set the grid-array with the number of the nodes horizontaly(nodesNumX) and vertically(nodesNumY)
        grid = new Node[nodesNumX, nodesNumY];

        //Gets the bottom left position from substracting the
        //left-half and the top-half part of the grid from the center position (transform.position)
        Vector2 bottomLeftPos = (Vector2)transform.position - Vector2.right * gridWorldSize.x / 2 - Vector2.up * gridWorldSize.y / 2;

        //Loop that create all the nodes of the grid and set their values
        for(int x=0; x<nodesNumX; ++x)
            for(int y=0; y < nodesNumY; ++y)
            {
                Vector2 thisNodePos = bottomLeftPos + Vector2.right * (x * nodeDiameter + nodeRadius) + Vector2.up * (y * nodeDiameter + nodeRadius);
                bool walkable = !Physics2D.OverlapCircle(thisNodePos, nodeRadius, unwalkableMask);
                
                int movementPenalty = 0;//Some regions has lower priorities than others

                if (walkable)
                {
                    //To know the territory of the node it is necesary to shoot a raycast in the Z axis and see what terrain is in the node position. 
                    //But all the components are 2D so with Physics2D.GetRayIntersection the code converts 3DRaycast into 2DRaycast more documentation in this page:
                    /*https://answers.unity.com/questions/1087239/get-2d-collider-with-3d-ray.html*/
                    Ray ray = new Ray((Vector3)thisNodePos + Vector3.back * 50, Vector3.forward); //shoot a raycast from top to down in the Z axis to get the region
                    
                    RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 100);
                    if (hit)
                        walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty); //Sets the movementPenalty of every walkable node
                }

                grid[x, y] = new Node(walkable, thisNodePos,x,y,movementPenalty);
            }
    }

    /// <summary> Return all the neighbours of a node (the nodes that are next to that node) </summary>
    public List<Node> GetNeighbours(Node node)
    {
        //list of the neighbours that the function is gonna return
        List<Node> neighbours = new List<Node>();

        //Checks the nodes that are neighbours (the nodes that are next in the positions left,right,top,bottom and the diagonals)
        for (sbyte x = -1; x <= 1; ++x)
            for (sbyte y = -1; y <= 1; ++y)
            {
                if (x == 0 && y == 0) continue; // x == 0 && y == 0 is the actual/selected node
                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                //Check if the index it isn't out of the range of the grid, and then add them to the list
                if (checkX >=0 && checkX < nodesNumX && checkY >=0 && checkY < nodesNumY)
                    neighbours.Add(grid[checkX, checkY]); 
            }
        return neighbours;
    }

    private void OnDrawGizmos()
    {
        //Draws the area of world that the grid is gonna cover
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, 1));

        //Draws all the nodes in the Gizmos with different colors
        if (grid != null && displayGridGizmos)
        {
            Node playerNode = FindNodeFromWorldPoint(player.position);
            foreach (Node node in grid)
            {
                if (node == playerNode)
                    Gizmos.color = Color.green;
                else if (node.walkable)
                    Gizmos.color = Color.white;
                else
                    Gizmos.color = Color.red;

                Gizmos.DrawCube(node.position, Vector3.one * (nodeDiameter - .1f));

            }
        }

    }

    /// <summary> Returns a node from a given worlds position </summary>
    public Node FindNodeFromWorldPoint(Vector3 worldPosition)
    {
        //Calculate the percent of the given "worldPosition" relative to the grid
        float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.y + gridWorldSize.y / 2) / gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);
        //Calculte the X and Y index of the node in the grid with the percentajes
        int x = Mathf.RoundToInt((nodesNumX - 1) * percentX);
        int y = Mathf.RoundToInt((nodesNumY - 1) * percentY);
        return grid[x, y]; //The grid gameobject have to be in position (0,0) to be correct. If it isn't then when player is near non walkable colliders 
        //it will appeared as if it is on top of them and bugg the enemy follow path
    }

    #endregion
}

#region Structs

//Struct to organice TerrainType variables 

/// <summary>There are different types of regions, with different priorities (Actually only 2): The normal with no penalty(0) and a ObjectColliderMargin.
/// It's used to make the enemy path dont go too close to avoid the rubbing/friction with objects </summary>
[System.Serializable]
public struct TerrainType
{
    public LayerMask terrainMask;
    public int terrainPenalty;
}

#endregion